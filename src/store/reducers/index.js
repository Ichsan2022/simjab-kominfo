import { combineReducers } from "redux";
import configReducers from "./config";

export default combineReducers({ 
	configReducers
});