import React, { PureComponent } from "react";
import { Redirect, Link } from "react-router-dom";
import { withRouter } from "react-router";
import {
  Button,
  Row,
  Col,
  Input,
  Layout,
  Form,
  Alert,
  Modal,
  message,
  Switch,
  Tabs,
  notification,
  Spin,
  Pagination,
  Tag
} from "antd";
import {
  TeamOutlined,
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  CloseSquareOutlined,
  ExclamationCircleOutlined,
  DiffOutlined,
  RedoOutlined
} from '@ant-design/icons';
// import "@ant-design/compatible/assets/index.css";
import Config from "../../Config";
import { Helmet } from "react-helmet";

import { connect } from "react-redux";
import { setConfig, ajaxHandler, ajaxViewHandler } from "../../store/actions";

import moment from "moment";

import axios from "axios";
import Cookies from "js-cookie";

const configClass = {
  apiUrl: {
    data: "api/cms_portal/user/",
  },
  urlPage: {
    data: "/admin/cms/user/form",
    name: "Data User"
  }
};

class DataUser extends PureComponent {
  config;
  timeoutGetQr;
  timeOutPushQr;

  constructor(props) {
    super(props);
    this.config = new Config();
    this.state = {
      errTitle: "",
      errMsg: "",
      loadingData: false,
      pagination: { pageSize: 10, current: 1, total: 0 },
      searchText: "",
      dataSource: [],
    };
  }

  componentDidMount() {
    this.getData();
  }

  async getData(
    limit = this.state.pagination.pageSize,
    page = this.state.pagination.current,
    search = this.state.searchText
  ) {
    this.setState({ loadingData: true, errTitle: "", errMsg: "" });
    //console.log('master/api/desa/?limit='+limit+'&page='+page);

    this.props
      .ajaxViewHandler(
        "get",
        configClass.apiUrl.data +
          "?limit=" +
          limit +
          "&page=" +
          page +
          "&search=" +
          search
      )
      .then(() => {
        setTimeout(() => {
          const data = this.props.responseMsg;
          if (this.props.isSuccess) {
            let pagination = { ...this.state.pagination };
            pagination.total = data.attr.total;
            this.setState(
              {
                dataSource: data.data,
                pagination,
              },
              () => {
                // console.log('pagination', this.state.pagination.total)
              }
            );
          } else {
            const errTitle = "error get data";
            this.setState({
              errTitle: errTitle,
              errMsg: data,
            });
          }
          this.setState({ loadingData: false });
        }, 500);
      })
      .catch((response) => {
        console.log("gagal_get_preview", response);
        this.setState({ loadingData: false });
      });
  }

  handleTableChange = async (page) => {
    let newPagination = this.state.pagination;
    newPagination.current = page;
    this.getData();

    // const newPagination = { ...this.state.pagination };
    // newPagination.pageSize = pagination.pageSize;
    // this.setState({
    //   loadingData: true,
    //   pagination,
    // });
    // if (pagination.current == 1) {
    //   this.getData(pagination.pageSize);
    // } else {
    //   this.getData(
    //     pagination.pageSize,
    //     (pagination.current - 1) * pagination.pageSize
    //   );
    // }
  };

  showDeleteConfirm(item) {
    Modal.confirm({
      title: 'Konfirmasi Hapus?',
      icon: <ExclamationCircleOutlined />,
      content: 'yakin menghapus data?',
      okText: 'Ya',
      okType: 'danger',
      cancelText: 'Tidak',
      onOk: () => this.prosesHapus(item),
      onCancel() {
        //console.log('Cancel');
      },
    });
  }

  prosesHapus(item) {
    this.setState({
      loadingData: true,
      errMsg: "",
      errTitle: "",
    })

    this.props.ajaxHandler('delete', configClass.apiUrl.data+item.id+'/')
    .then(() =>{
      if(this.props.isSuccess){
        notification.success({
            message: "sukses",
            description: "berhasil menghapus.",
            placement: "bottomRight",
          });
        this.getData();
      }else{
        this.setState({
          errTitle: "gagal menghapus",
          errMsg: JSON.stringify(this.props.responseMsg),
          loadingData: false
        });
      }
    }).catch((response) => {
      this.setState({
        loadingData: false,
        errTitle: "gagal menghapus",
        errMsg: JSON.stringify(response)
      });
    });
  }

  render() {
    return (
      <>
        {/*<!-- BEGIN: Content-->*/}
        <div class="app-content content ">
          <div class="content-overlay"></div>
          <div class="header-navbar-shadow"></div>
          <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
              <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                  <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">
                      {configClass.urlPage.name}
                    </h2>
                    <div class="breadcrumb-wrapper">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                          <Link to={"/admin/index"}>
                            <a>Home</a>
                          </Link>
                        </li>
                        <li class="breadcrumb-item active">{configClass.urlPage.name}</li>
                      </ol>
                    </div>
                  </div>
                </div>
              </div>
              
            </div>
            <div class="content-body">
              {/*<!-- Table head options start -->*/}
              <div class="row" id="table-head">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">{configClass.urlPage.name}</h4>
                    </div>
                    <div class="card-body">
                      {this.state.errMsg != "" &&
                        
                          <div className="alert alert-danger alert-dismissible fade show" role="alert">
                            <div className="alert-body d-flex align-items-center">
                              {this.state.errMsg}
                            </div>
                          </div>
                        
                      }
                      <Spin tip="Loading..." spinning={this.state.loadingData}>
                        <div>
                          <div style={{float: "right"}}>
                            <Input.Search 
                              size="large"
                              allowClear
                              placeholder="search..." 
                              onSearch={ val => {
                                let tempPage = this.state.pagination;
                                tempPage.current = 1;
                                this.setState({
                                  searchText: val,
                                  pagination: tempPage
                                }, () =>{
                                  this.getData();
                                });
                              }} 
                              className="mb-1 d-inline"
                              style={{ width: 200, float: "left", marginRight: 10}} 
                            />
                            <Link to={configClass.urlPage.data}>
                              <button
                                className="btn btn-primary d-inline"
                              >
                                <i
                                  class="me-1"
                                  dangerouslySetInnerHTML={{
                                    __html: window.feather.icons["plus-square"].toSvg(),
                                  }}
                                />
                                Tambah Data
                              </button>
                            </Link>
                          </div>

                          <div class="table-responsive" style={{clear: "both"}}>
                          <table class="table table-striped">
                            <thead class="table-dark">
                              <tr>
                                <th width={300}>AKSI</th>
                                <th>ID</th>
                                <th>NAME</th>
                                <th>EMAIL</th>
                                <th>STATUS</th>
                                <th>IS SUPER ADMIN</th>
                                <th>CREATED AT</th>
                                <th>CREATED BY</th>
                                <th>UPDATED AT</th>
                                <th>UPDATED BY</th>
                                <th>DELETED AT</th>
                                <th>DELETED BY</th>
                              </tr>
                            </thead>
                            <tbody>
                              {this.state.dataSource.length == 0 && (
                                <tr>
                                  <td colSpan={11}>belum ada data</td>
                                </tr>
                              )}
                              {this.state.dataSource.map((item, idx) => (
                                <tr key={"row" + item.id}>
                                  <td>
                                        
                                         <div style={{marginRight: 10, float: "left"}}>
                                           <Link to={configClass.urlPage.data+"/"+item.id}>
                                             <i
                                                class="me-50"
                                                dangerouslySetInnerHTML={{
                                                  __html:
                                                    window.feather.icons[
                                                      "edit"
                                                    ].toSvg(),
                                                }}

                                              />
                                            </Link>
                                          </div>
                                          <div style={{marginRight: 10, float: "left"}}>
                                            <i
                                              class="me-50 text-danger"
                                              dangerouslySetInnerHTML={{
                                                __html:
                                                  window.feather.icons[
                                                    "trash"
                                                  ].toSvg(),
                                              }}
                                              onClick={() => this.showDeleteConfirm(item)}
                                            />
                                          </div>
                                    
                                  </td>
                                  <td>{item.id}</td>
                                  <td>{item.name}</td>
                                  <td>{item.email}</td>
                                  <td>
                                    {
                                      item.status
                                      ?
                                        <Tag color="success">true</Tag>
                                      :
                                        <Tag color="error">false</Tag>

                                    }
                                  </td>
                                  <td>
                                    {
                                      item.is_superadmin
                                      ?
                                        <Tag color="success">true</Tag>
                                      :
                                        <Tag color="error">false</Tag>

                                    }
                                  </td>
                                  <td>{moment(new Date(item.created_at)).format("DD-MM-YYYY HH:mm")}</td>
                                  <td>{item.created_by_nama}</td>
                                  <td>{item.updated_at == null ? "-" : moment(new Date(item.updated_at)).format("DD-MM-YYYY HH:mm")}</td>
                                  <td>{item.updated_by_nama == null ? "-" : moment(new Date(item.updated_by_nama)).format("DD-MM-YYYY HH:mm")}</td>
                                  <td>{item.deleted_at == null ? "-" : moment(new Date(item.deleted_at)).format("DD-MM-YYYY HH:mm")}</td>
                                  <td>{item.deleted_by_nama == null ? "-" : moment(new Date(item.deleted_by_nama)).format("DD-MM-YYYY HH:mm")}</td>
                                </tr>
                              ))}
                            </tbody>
                          </table>
                          </div>
                          <Pagination
                            // defaultCurrent={1}
                            // pageSize={this.state.pagination.pageSize}
                            // total={this.state.pagination.total}

                            onChange={this.handleTableChange}
                            defaultCurrent={1}
                            current={this.state.pagination.current}
                            total={this.state.pagination.total}
                            hideOnSinglePage
                            pageSize={this.state.pagination.pageSize}
                            showSizeChanger={false}
                            className="mt-1"
                            style={{float: "right"}}
                            // itemRender={
                            //   (page, type, originalElement) => {
                            //     if (type === 'prev') {
                            //       return <a>Previous</a>;
                            //     }
                            //     if (type === 'next') {
                            //       return <a>Next</a>;
                            //     }
                            //     if (type === 'page') {
                            //       // return 'page '+page
                            //       return(
                            //         <li class="page-item"><a class="page-link" href="#">{page}</a></li>
                            //       )
                            //     }
                            //     // return originalElement;
                            //   }                              
                            // }
                          />
                        </div>
                      </Spin>
                    </div>
                  </div>
                </div>
              </div>
              {/*<!-- Table head options end -->*/}
            </div>
          </div>
        </div>
        {/*<!-- END: Content-->*/}
      </>
    );
  }
}

const mapStateToProps = function (state) {
  return state.configReducers;
};

export default connect(mapStateToProps, {
  setConfig,
  ajaxHandler,
  ajaxViewHandler,
})(withRouter(DataUser));
