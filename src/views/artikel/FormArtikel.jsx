import React, { PureComponent } from "react";
import { Redirect, Link } from "react-router-dom";
import { withRouter } from "react-router";
import {
  Button,
  Row,
  Col,
  Input,
  Layout,
  Form,
  Alert,
  Modal,
  message,
  Switch,
  Tabs,
  notification,
  Spin,
  Pagination,
  Radio,
  Select,
} from "antd";
import {
  TeamOutlined,
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  CloseSquareOutlined,
  ExclamationCircleOutlined,
  DiffOutlined,
  RedoOutlined
} from '@ant-design/icons';
// import "@ant-design/compatible/assets/index.css";
import Config from "../../Config";
import { Helmet } from "react-helmet";
import moment from "moment";

import { connect } from "react-redux";
import { setConfig, ajaxHandler, ajaxViewHandler } from "../../store/actions";
import { Editor } from "@tinymce/tinymce-react";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import axios from "axios";
import Cookies from "js-cookie";
import slugify from "react-slugify";

const configClass = {
  apiUrl: {
    preview: "api/cms_portal/content/",
    gbr: "api/cms_portal/content/image/",
    tags: "api/cms_portal/tag/",
  },
  urlPage: {
    data: "/admin/cms/artikel",
    name: "Data Artikel",
    form: "Form Artikel",
  },
  limit: 20,
};

class FormArtikel extends PureComponent {
  formRef = React.createRef();
  config;
  timeoutGetQr;
  timeOutPushQr;

  constructor(props) {
    super(props);
    this.config = new Config();
    this.state = {
      errComponent: false,
      errTitle: "",
      errMsg: "",
      loadingData: false,

      aksi: "tambah",
      method: "post",
      idLama: "",

      artikelString: "",

      preview_gambar: "",

      src: [],
      crop: {
        unit: "%",
        width: 100,
        aspect: 16 / 9,
      },
      gambar_jadi: [],
      type_gambar: [],
      name_gambar: [],
      croppedImageUrl: [],

      is_banner: 0,
      is_main: 0,

      modalCrop: false,

      posisiGbr: 0,

      gbr_post: [],

      status: true,
      is_slide_banner: false,

      loadingTags: false,
      listTags: null,

      isSukses: false,

      gambar_edit: [],
    };
  }

  componentDidMount() {
    if (this.props.match.params.id != undefined) {
      this.setState({
        loadingData: true
      });
    }
    this.loadMasterList();
  }

  loadMasterList() {
    const rule = [
      {
        destination: {
          stateProgressName: "loadingTags",
          endPoint: configClass.apiUrl.tags + "?limit=" + configClass.limit,
          stateDestination: "listTags",
        },
        nextDestination: null,
        // nextDestination: {
        //  stateProgressName: 'loadingGudang',
        //  endPoint: configClass.apiUrl.gudang,
        //  stateDestination: 'listGudang',
        //  next: null
        // }
      },
    ];

    this.setState({
      errComponent: false,
    });

    rule.map((item, index) => {
      if (index == rule.length - 1) {
        this.getMasterList(item, true);
      } else {
        this.getMasterList(item);
      }
    });
  }

  async getMasterList(rule, isFinal = false) {
    var destination = rule.destination;
    var nextDestination = rule.nextDestination;
    this.setState({ [destination.stateProgressName]: true });
    this.props
      .ajaxViewHandler("get", destination.endPoint)
      .then(() => {
        setTimeout(() => {
          const data = this.props.responseMsg;
          if (this.props.isSuccess) {
            if (data.attr != undefined) {
              let newState = null;
              if (this.state[destination.stateDestination] == null) {
                newState = data;
              } else {
                if (destination.stateProgressName.includes("_searching")) {
                  newState = data;
                } else {
                  const oldState = this.state[destination.stateDestination];
                  newState = {
                    attr: data.attr,
                    data: [...oldState.data, ...data.data],
                  };
                }
              }
              // console.log('state_name', destination.stateDestination);
              // console.log('state_value', newState);
              this.setState({
                [destination.stateDestination]: newState,
              });
            }
            if (isFinal) {
              if (this.props.match.params.id != undefined) {
                this.preview(this.props.match.params.id);
              }
            }
          } else {
            this.setState({
              errComponent: true,
            });
          }
          this.setState({ [destination.stateProgressName]: false });
        }, 500);
      })
      .catch((response) => {
        //console.log('gagal_get_master', response);
        message.error({
          content: "gagal request data, coba lagi",
          duration: 2,
          key: "toast",
        });
        this.setState({
          errTitle: "gagal request data master",
          errMsg: response,
        });
        this.setState({ [destination.stateProgressName]: false });
      });
    //next function
    if (nextDestination != null) {
      const next = {
        destination: {
          stateProgressName: nextDestination.stateProgressName,
          endPoint: nextDestination.endPoint,
          stateDestination: nextDestination.stateDestination,
        },
        nextDestination: nextDestination.next,
      };
      this.getMasterList(next);
    }
  }

  onSelectFile = (e, idx) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        let tempSrc = this.state.src;
        if (this.state.src[idx] == undefined) {
          tempSrc = [...tempSrc, reader.result];
        } else {
          tempSrc[idx] = reader.result;
        }
        // this.setState({ src: reader.result })
        this.setState({ src: tempSrc, posisiGbr: idx });
      });
      reader.readAsDataURL(e.target.files[0]);

      // let tempType = this.state.type_gambar;
      // tempType = [...tempType, e.target.files[0].type];

      if (this.state.gambar_jadi[idx] == undefined) {
        const tempGbrJadi = [...this.state.gambar_jadi, null];
        const tempTypeGbr = [...this.state.type_gambar, e.target.files[0].type];
        const tempNameGbr = [...this.state.name_gambar, e.target.files[0].name];
        const tempCroppedImageUrl = [...this.state.croppedImageUrl, null];

        // console.log('type_gambar', e.target.files[0].type);

        this.setState({
          modalCrop: true,

          gambar_jadi: tempGbrJadi,
          type_gambar: tempTypeGbr,
          name_gambar: tempNameGbr,
          croppedImageUrl: tempCroppedImageUrl,
        });
      } else {
        let tempTypeGbr = this.state.type_gambar;
        tempTypeGbr[idx] = e.target.files[0].type;

        let tempNameGbr = this.state.name_gambar;
        tempNameGbr[idx] = e.target.files[0].name;

        // console.log('type_gambar', e.target.files[0].type);

        this.setState({
          modalCrop: true,
          type_gambar: tempTypeGbr,
          name_gambar: tempNameGbr,
          is_banner: 0,
          is_main: 0,
        });
      }
    }
  };

  onImageLoaded = (image) => {
    this.imageRef = image;
  };

  onCropComplete = (crop) => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop) => {
    // You could also use percentCrop:
    // this.setState({ crop: percentCrop });
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        this.state.name_gambar[this.state.posisiGbr]
      );

      // let tempcroppedImageUrl = this.state.croppedImageUrl;
      // tempcroppedImageUrl = [...tempcroppedImageUrl, croppedImageUrl];
      // this.setState({ croppedImageUrl: tempcroppedImageUrl });
    }
  }

  getCroppedImg(image, crop, fileName) {
    var canvas = document.createElement("canvas");
    canvas.setAttribute("id", "gbr-jadi");
    const pixelRatio = window.devicePixelRatio;
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext("2d");

    canvas.width = crop.width * pixelRatio * scaleX;
    canvas.height = crop.height * pixelRatio * scaleY;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = "high";

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width * scaleX,
      crop.height * scaleY
    );

    var that = this;

    return new Promise((resolve, reject) => {
      canvas.toBlob(
        (blob) => {
          if (!blob) {
            //reject(new Error('Canvas is empty'));
            console.error("Canvas is empty");
            return;
          }

          blob.name = fileName;
          window.URL.revokeObjectURL(this.fileUrl);
          this.fileUrl = window.URL.createObjectURL(blob);

          // document.getElementById("pilihGbr"+that.state.posisiGbr).src = this.fileUrl

          // console.log('fileurl', this.fileUrl)
          let tempGbrJadi = that.state.gambar_jadi;
          tempGbrJadi[that.state.posisiGbr] = blob;

          let tempcroppedImageUrl = that.state.croppedImageUrl;
          tempcroppedImageUrl[that.state.posisiGbr] = this.fileUrl;

          that.setState({
            gambar_jadi: tempGbrJadi,
            croppedImageUrl: tempcroppedImageUrl,
          });
          resolve(this.fileUrl);
        },
        that.state["type_gambar"][that.state.posisiGbr],
        1
      );
    });
  }

  handleEditorChange = (content, editor) => {
    this.setState({
      artikelString: content,
    });
  };

  prosesSimpanGbr = () => {
    var datas = new FormData();
    if (this.state.gambar_jadi[this.state.posisiGbr] != null) {
      datas.append("name", this.state.name_gambar[this.state.posisiGbr]);
      datas.append(
        "image",
        this.state.gambar_jadi[this.state.posisiGbr],
        this.state.gambar_jadi[this.state.posisiGbr]["name"]
      );
      datas.append("is_banner", this.state.is_banner);
      datas.append("is_main", this.state.is_main);
    }

    // console.log(datas);
    this.postGambar(datas);
  };

  handleSubmit = (values) => {
    var datas = {};
    for (var key in values) {
      if (key == "tags") {
        let temp = [];
        values[key].map((item, idx) => {
          if (item.label == undefined) {
            //tag baru
            temp = [
              ...temp,
              {
                id: 0,
                name: item.value,
              },
            ];
          } else {
            //tag lama
            temp = [
              ...temp,
              {
                id: item.value,
                name: item.label,
              },
            ];
          }
        });
        datas = { ...datas, tag: temp };
      } else {
        if (values[key] == undefined) {
          datas = { ...datas, [key]: "" };
        } else {
          // datas.append(key, values[key].toString());
          datas = { ...datas, [key]: values[key].toString() };
        }
      }
    }
    datas = { ...datas, category_id: 2 };
    datas = { ...datas, image: this.state.gbr_post };
    datas = { ...datas, file: [] };
    datas = { ...datas, body: this.state.artikelString };
    this.postData(datas);
  };

  async postGambar(datas) {
    this.setState({
      loadingData: true,
      errTitle: "",
      errMsg: "",
    });
    // message.loading({ content: 'Memproses...', key: 'toast' });

    this.props
      .ajaxHandler("post", configClass.apiUrl.gbr, datas, true, true)
      .then(() => {
        setTimeout(() => {
          this.setState({
            loadingData: false,
          });
          const data = this.props.responseMsg;
          if (this.props.isSuccess) {
            let tempGbr = [
              ...this.state.gbr_post,
              {
                id: data.data.id,
                name: data.data.name,
              },
            ];
            document.getElementById("pilihGbr" + this.state.posisiGbr).src =
              this.state.croppedImageUrl[this.state.posisiGbr];
            this.setState({
              modalCrop: false,
              gbr_post: tempGbr,
            });
          } else {
            const errTitle = "gagal upload gambar";
            this.setState({
              errTitle: errTitle,
              errMsg: JSON.stringify(this.props.responseMsg),
              modalCrop: false,
            });
          }
        }, 500);
      })
      .catch((response) => {
        this.setState({
          loadingData: false,
          errTitle: "gagal upload gambar",
          errMsg: JSON.stringify(response),
          modalCrop: false,
        });
      });
  }

  async postData(datas) {
    this.setState({
      loadingData: true,
      errTitle: "",
      errMsg: "",
    });
    // message.loading({ content: 'Memproses...', key: 'toast' });

    this.props
      .ajaxHandler(
        this.state.method,
        configClass.apiUrl.preview + this.state.idLama,
        datas,
        false,
        true
      )
      .then(() => {
        setTimeout(() => {
          this.setState({
            loadingData: false,
          });
          const data = this.props.responseMsg;
          if (this.props.isSuccess) {
            notification.success({
              message: "sukses",
              description: "berhasil menyimpan.",
              placement: "bottomRight",
            });
            this.setState({
              isSukses: true,
            });
          } else {
            const errTitle = "gagal menyimpan data";
            this.setState({
              errTitle: errTitle,
              errMsg: JSON.stringify(this.props.responseMsg),
            });
          }
        }, 500);
      })
      .catch((response) => {
        this.setState({
          loadingData: false,
          errTitle: "gagal menyimpan",
          errMsg: JSON.stringify(response),
        });
      });
  }

  async preview(id) {
    this.setState({
      loadingData: true,
      errTitle: "",
      errMsg: "",
    });

    this.props
      .ajaxViewHandler("get", configClass.apiUrl.preview + id)
      .then(() => {
        setTimeout(() => {
          const data = this.props.responseMsg.data;
          if (this.props.isSuccess) {
            this.formRef.current.setFieldsValue(data);
            for (var key in data) {
              const dateRegex = /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/;
              const dateTimeRegex =
                /^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}\:[0-9]{2}\:[0-9]{2}$/;
              if (dateRegex.test(data[key])) {
                //jika format tanggal
                this.formRef.current.setFieldsValue({
                  [key + "_picker"]: moment(new Date(data[key]), "DD-MM-YYYY"),
                  [key]: moment(new Date(data[key]), "DD-MM-YYYY"),
                });
              } else if (dateTimeRegex.test(data[key])) {
                //jika format tanggal dan jam
                this.formRef.current.setFieldsValue({
                  [key + "_picker"]: moment(
                    new Date(data[key]),
                    "DD-MM-YYYY HH:mm:ss"
                  ),
                  [key]: moment(new Date(data[key]), "DD-MM-YYYY HH:mm:ss"),
                });
              } else {
                if (key == "body") {
                  this.setState({
                    artikelString: data[key],
                  });
                  this.formRef.current.setFieldsValue({
                    content_editor: data[key],
                    body_editor: data[key],
                  });
                } else if (key == "tag") {
                  let temp = [];
                  data[key].map((item) => {
                    temp = [
                      ...temp,
                      {
                        value: item.id,
                        label: item.nama,
                        key: item.id,
                      },
                    ];
                  });
                  this.formRef.current.setFieldsValue({
                    tags: temp,
                  });
                } else if (key == "image") {
                  let temp = [];
                  let tempGb = [];
                  data[key].map((item) => {
                    temp = [
                      ...temp,
                      {
                        id: item.id,
                        name: item.name,
                      },
                    ];
                    tempGb = [...tempGb, {
                      id: item.id,
                      name: item.name,
                      path: item.path,
                    }];
                  });
                  this.setState({
                    gbr_post: temp,
                    gambar_edit: tempGb,
                  });
                } else {
                  this.formRef.current.setFieldsValue({
                    [key]: data[key],
                  });
                }
              }

              // if (key == "icon") {
              //   this.setState({
              //     preview_gambar: data[key],
              //   });
              // }
            }
            message.destroy();
            this.setState({
              aksi: "edit",
              method: "put",
              idLama: data.id + "/",
              loadingData: false,
            });
          } else {
            const errTitle = "error preview data lama";
            this.setState({
              errTitle: errTitle,
              errMsg: JSON.stringify(data),
              loadingData: false,
            });
          }
        }, 500);
      })
      .catch((response) => {
        this.setState({
          errTitle: "gagal get data",
          errMsg: JSON.stringify(response),
          loadingData: false,
        });
      });
  }

  generateEmptySelect() {
    return <Select placeholder="pilih" optionFilterProp="children"></Select>;
  }

  generateSelectMultiple(stateName, url, key, value) {
    // console.log("tes", url);
    return this.state["loading" + stateName] ? (
      <img src={process.env.PUBLIC_URL + "/loading_crop.gif"} height="50" />
    ) : (
      <Select
        labelInValue
        mode="tags"
        showSearch
        placeholder="pilih"
        optionFilterProp="children"
        onSearch={(val) => {
          const rule = {
            destination: {
              stateProgressName: "loading" + stateName + "_searching",
              endPoint: url + "?limit=1000" + "&page=1&search=" + val,
              stateDestination: "list" + stateName,
            },
            nextDestination: null,
          };
          this.getMasterList(rule);
        }}
        onPopupScroll={(e) => {
          e.persist();
          let target = e.target;
          if (target.scrollTop + target.offsetHeight === target.scrollHeight) {
            const curPage = this.state["list" + stateName].attr.current_page;
            const cekLast =
              this.state["list" + stateName].attr.total /
                this.state["list" + stateName].attr.per_page -
              this.state["list" + stateName].attr.current_page;
            if (cekLast > 0) {
              const rule = {
                destination: {
                  stateProgressName: "loading" + stateName + "Paging",
                  endPoint:
                    url +
                    "?limit=" +
                    configClass.limit +
                    "&page=" +
                    this.state["list" + stateName].next,
                  stateDestination: "list" + stateName,
                },
                nextDestination: null,
              };
              this.getMasterList(rule);
            }
          }
        }}
      >
        {this.state["loading_" + stateName + "searching"] && (
          <Select.Option value="">
            <Spin size="small" />
          </Select.Option>
        )}

        {this.state["list" + stateName] != null &&
          this.state["list" + stateName]["data"].map((item) => (
            <Select.Option value={item[key]}>{item[value] + ""}</Select.Option>
          ))}
        {this.state["list" + stateName] != undefined &&
          this.state["list" + stateName].next != undefined && (
            <Select.Option value="">
              <Spin size="small" />
            </Select.Option>
          )}
      </Select>
    );
  }

  showDeleteConfirm(id) {
    Modal.confirm({
      title: 'Konfirmasi Hapus?',
      icon: <ExclamationCircleOutlined />,
      content: 'yakin menghapus gambar?',
      okText: 'Ya',
      okType: 'danger',
      cancelText: 'Tidak',
      onOk: () => this.prosesHapus(id),
      onCancel() {
        //console.log('Cancel');
      },
    });
  }

  prosesHapus(id) {
    this.props.ajaxHandler('delete', configClass.apiUrl.gbr+id+'/')
      .then(() =>{
        setTimeout(() =>{
          if(this.props.isSuccess){
            let temp = [];
            let tempGb = [];
            this.state.gambar_edit.map(item =>{
              if(item.id != id){
                temp = [...temp, item];
              }
            })
            this.state.gbr_post.map(item =>{
              if(item.id != id){
                tempGb = [...tempGb, item];
              }
            })
            // alert(JSON.stringify(temp))
            this.setState({
              gambar_edit: temp,
              gbr_post: tempGb
            });
          }else{
            this.setState({
              errTitle: "gagal menghapus",
              errMsg: JSON.stringify(this.props.responseMsg),
              loadingData: false
            });
          }
        }, 500);
      }).catch((response) => {
        this.setState({
          loadingData: false,
          errTitle: "gagal menghapus",
          errMsg: JSON.stringify(response)
        });
      });
  }

  render() {
    if (this.state.isSukses) {
      return <Redirect to={configClass.urlPage.data} />;
    }

    // const { crop, croppedImageUrl, src } = this.state;

    return (
      <>
        {/*<!-- BEGIN: Content-->*/}
        <div class="app-content content ">
          <div class="content-overlay"></div>
          <div class="header-navbar-shadow"></div>
          <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
              <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                  <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">
                      {configClass.urlPage.form}
                    </h2>
                    <div class="breadcrumb-wrapper">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                          <Link to={"/admin/index"}>
                            <a>Home</a>
                          </Link>
                        </li>
                        <li class="breadcrumb-item">
                          <Link to={configClass.urlPage.data}>
                            <a>{configClass.urlPage.name}</a>
                          </Link>
                        </li>
                        <li class="breadcrumb-item active">
                          {configClass.urlPage.form}
                        </li>
                      </ol>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-body">
              {/*<!-- Table head options start -->*/}
              <div class="row" id="table-head">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">{configClass.urlPage.form}</h4>
                    </div>
                    <div class="card-body">
                      {this.state.errMsg != "" && (
                        <div
                          className="alert alert-danger alert-dismissible fade show"
                          role="alert"
                        >
                          <div className="alert-body d-flex align-items-center">
                            {this.state.errMsg}
                          </div>
                        </div>
                      )}
                      <Spin tip="Loading..." spinning={this.state.loadingData}>
                        <Form
                          // layout="horizontal"
                          onFinish={this.handleSubmit}
                          size="small"
                          ref={this.formRef}
                          size="default"
                          initialValues={{
                            status: this.state.status,
                            is_slide_banner: this.state.is_slide_banner,
                          }}
                        >
                          <div class="mb-1 row">
                            <label for="name" class="col-12">
                              Title
                            </label>
                            <div class="col-12">
                              <Form.Item
                                name="title"
                                rules={[
                                  {
                                    required: true,
                                    message: "harus diisi",
                                  },
                                ]}
                              >
                                <Input
                                  type="text"
                                  id="name"
                                  class="form-control"
                                  placeholder="input title"
                                  onChange={(e) => {
                                    this.formRef.current.setFieldsValue({
                                      slug: slugify(e.target.value),
                                    });
                                  }}
                                />
                              </Form.Item>
                            </div>
                          </div>
                          <div class="mb-1 row">
                            <label for="slug" class="col-12">
                              Slug
                            </label>
                            <div class="col-12">
                              <Form.Item
                                name="slug"
                                rules={[
                                  {
                                    required: true,
                                    message: "harus diisi",
                                  },
                                ]}
                              >
                                <Input
                                  type="text"
                                  id="slug"
                                  class="form-control"
                                  placeholder="input slub"
                                />
                              </Form.Item>
                            </div>
                          </div>
                          <div class="mb-1 d-flex flex-row">
                            {/*<label for="name" class="col-4">
                              Status
                            </label>
                            <label for="name" class="col-8">
                              Is Slide Banner
                            </label>*/}
                            <div style={{ marginRight: 10 }}>
                              <Form.Item
                                name="status"
                                rules={[
                                  {
                                    required: true,
                                    message: "harus diisi",
                                  },
                                ]}
                              >
                                <Radio.Group
                                  onChange={(e) => {
                                    this.setState({
                                      status: e.target.value,
                                    });
                                  }}
                                  defaultValue={this.state.status}
                                  value={this.state.status}
                                  optionType="button"
                                  buttonStyle="solid"
                                >
                                  <Radio value={true}>Aktif</Radio>
                                  <Radio value={false}>Non Aktif</Radio>
                                </Radio.Group>
                              </Form.Item>
                            </div>
                            <div>
                              <Form.Item
                                name="is_slide_banner"
                                rules={[
                                  {
                                    required: true,
                                    message: "harus diisi",
                                  },
                                ]}
                              >
                                <Radio.Group
                                  onChange={(e) => {
                                    this.setState({
                                      status: e.target.value,
                                    });
                                  }}
                                  optionType="button"
                                  buttonStyle="solid"
                                >
                                  <Radio value={true}>Banner Aktif</Radio>
                                  <Radio value={false}>Banner Non Aktif</Radio>
                                </Radio.Group>
                              </Form.Item>
                            </div>
                          </div>
                          <div class="mb-1 row">
                            <label for="name" class="col-12">
                              Tag
                            </label>
                            <div class="col-12">
                              <Form.Item
                                name="tags"
                                rules={[
                                  {
                                    required: true,
                                    message: "harus diisi",
                                  },
                                ]}
                              >
                                {this.state.listTags != null
                                  ? this.generateSelectMultiple(
                                      "Tags",
                                      configClass.apiUrl.tags,
                                      "id",
                                      "name"
                                    )
                                  : this.generateEmptySelect()}
                              </Form.Item>
                            </div>
                          </div>
                          {/*<div class="mb-1 row">
                            <label for="name" class="col-12">
                              Is Slide Banner
                            </label>
                            <div class="col-12">
                              <Form.Item
                                name="is_slide_banner"
                                rules={[
                                  {
                                    required: true,
                                    message: "harus diisi",
                                  },
                                ]}
                              >
                                <Radio.Group
                                  onChange={(e) => {
                                    this.setState({
                                      status: e.target.value,
                                    });
                                  }}
                                  optionType="button"
                                  buttonStyle="solid"
                                >
                                  <Radio value={true}>True</Radio>
                                  <Radio value={false}>False</Radio>
                                </Radio.Group>
                              </Form.Item>
                            </div>
                          </div>*/}
                          <div class="mb-1 row">
                            <label for="name" class="col-12">
                              Gambar
                            </label>
                            <div class="col-12">
                              {this.state.aksi == "edit" &&
                                this.state.gambar_edit.map((item, idx) => (
                                  <div
                                    key={"gbredit-" + idx}
                                    className="d-inline-block"
                                    style={{ marginRight: 5 }}
                                  >
                                    <div 
                                      className="add-image"
                                      style={{
                                        position: "relative"
                                      }}
                                    >
                                      <img width={100} src={item.path} />
                                      <i
                                        class="me-50"
                                        dangerouslySetInnerHTML={{
                                          __html:
                                            window.feather.icons[
                                              "trash"
                                            ].toSvg(),
                                        }}
                                        style={{
                                          position: "absolute",
                                          top: 0,
                                          right: 0,
                                          background: "rgba(246, 16, 16, 0.5)",
                                          padding: 2,
                                          margin: 2,
                                          color: "#fff",
                                          borderRadius: 3,
                                          cursor: "pointer"
                                        }}
                                        title="hapus gambar"
                                        onClick={() => this.showDeleteConfirm(item.id)}
                                      />
                                    </div>
                                  </div>
                                ))}
                              {Array.from({
                                length: this.state.src.length + 1,
                              }).map((_, i) => (
                                <div
                                  key={"gbr-" + i}
                                  className="d-inline-block"
                                  style={{ marginRight: 5 }}
                                >
                                  <div className="add-image">
                                    <img
                                      id={"pilihGbr" + i}
                                      width={100}
                                      src={
                                        process.env.PUBLIC_URL +
                                        "/add_image.png"
                                      }
                                      onClick={(e) => {
                                        document
                                          .getElementById("upload" + i)
                                          .click();
                                      }}
                                      title={"pilih gambar " + (i + 1)}
                                    />
                                  </div>
                                  <Input
                                    style={{ display: "none" }}
                                    id={"upload" + i}
                                    accept="image/png, image/jpeg"
                                    type="file"
                                    placeholder="pilih gambar"
                                    // onChange={this.setGambar}
                                    onChange={(e) => this.onSelectFile(e, i)}
                                  />
                                </div>
                              ))}
                            </div>
                          </div>
                          <div class="mb-1 row">
                            <label for="name" class="col-12">
                              Body
                            </label>
                            <div class="col-12">
                              {/*<Form.Item
                                name="body_editor"
                                rules={[
                                  {
                                    required: true,
                                    message: "harus diisi",
                                  },
                                ]}
                              >*/}
                                <Editor
                                  //initialValue="<p>This is the initial content of the editor</p>"
                                  name="body_editor"
                                  apiKey="ovy1v97bvfyn3h3fhjm98i32q6285p9ouzej95q5332jgcf9"
                                  init={{
                                    // extended_valid_elements : "img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name]",
                                    height: 500,
                                    menubar: true,
                                    plugins: [
                                      "advlist autolink lists link image charmap print preview anchor",
                                      "searchreplace visualblocks code fullscreen",
                                      "insertdatetime media table paste code help wordcount",
                                    ],
                                    toolbar:
                                      "undo redo | formatselect | bold italic backcolor | \
                                           alignleft aligncenter alignright alignjustify | \
                                           bullist numlist outdent indent | removeformat | help",
                                    //toolbar2: "print preview media | forecolor backcolor emoticons",
                                    image_advtab: true,
                                    file_picker_callback: function (
                                      callback,
                                      value,
                                      meta
                                    ) {
                                      if (meta.filetype == "image") {
                                        document
                                          .getElementById("upload")
                                          .click();
                                        document
                                          .getElementById("upload")
                                          .addEventListener(
                                            "change",
                                            function () {
                                              var file = this.files[0];
                                              var reader = new FileReader();
                                              reader.onload = function (e) {
                                                callback(e.target.result, {
                                                  alt: "",
                                                });
                                              };
                                              reader.readAsDataURL(file);
                                            }
                                          );
                                      }
                                    },
                                  }}
                                  // initialValue={this.state.artikelString}
                                  value={this.state.artikelString}
                                  onEditorChange={this.handleEditorChange}
                                />
                              {/*</Form.Item>*/}
                              <input
                                name="image"
                                type="file"
                                id="upload"
                                class="hidden"
                                onchange=""
                                style={{ display: "none" }}
                              />
                            </div>
                          </div>
                          <button class="btn btn-primary" type="submit">
                            Submit
                          </button>
                          <Link to={configClass.urlPage.data}>
                            <button
                              class="btn btn-danger"
                              type="button"
                              style={{ marginLeft: 10 }}
                            >
                              Cancel
                            </button>
                          </Link>
                        </Form>
                      </Spin>
                    </div>
                  </div>
                </div>
              </div>
              {/*<!-- Table head options end -->*/}
            </div>
          </div>
        </div>
        {/*<!-- END: Content-->*/}

        <Modal
          zIndex={1000}
          width={"80%"}
          title={"GESER DAN SESUAIKAN LOKASI GAMBAR"}
          closable={false}
          maskClosable={false}
          visible={this.state.modalCrop}
          // onCancel={() => {
          //   this.setState({
          //     modalCrop: false,
          //   });
          // }}
          footer={[
            <button
              disabled={this.state.loadingData ? true : false}
              key="btn-simpan-gbr"
              className="btn btn-primary"
              onClick={() => {
                // var img = document.createElement('img');
                // img.src = url;

                // var image = new Image();
                // image.src = URL.createObjectURL(this.state.croppedImageUrl[this.state.posisiGbr]);

                // let url = window.URL.createObjectURL(URL.createObjectURL());

                this.prosesSimpanGbr();
              }}
            >
              Simpan
            </button>,
          ]}
        >
          <Spin tip="Loading..." spinning={this.state.loadingData}>
            {this.state.src[this.state.posisiGbr] && (
              <>
                <div class="mb-1 row">
                  <label for="name" class="col-12">
                    Name
                  </label>
                  <div class="col-12">
                    <Input
                      type="text"
                      id="name"
                      class="form-control"
                      placeholder="input nama"
                      defaultValue={
                        this.state.name_gambar[this.state.posisiGbr].split(
                          "."
                        )[0]
                      }
                      // value={this.state.name_gambar[this.state.posisiGbr].split(".")[0]}
                      onChange={(e) => {
                        var temp = this.state.name_gambar;
                        temp[this.state.posisiGbr] = e.target.value;
                        this.setState({
                          name_gambar: temp,
                        });
                      }}
                    />
                  </div>
                </div>
                <div class="mb-1 row">
                  <label for="is_banner" class="col-12">
                    Is Banner
                  </label>
                  <div class="col-12">
                    <Radio.Group
                      onChange={(e) => {
                        this.setState({
                          is_banner: e.target.value,
                        });
                      }}
                      defaultValue={this.state.is_banner}
                      value={this.state.is_banner}
                      optionType="button"
                      buttonStyle="solid"
                    >
                      <Radio value={1}>True</Radio>
                      <Radio value={0}>False</Radio>
                    </Radio.Group>
                  </div>
                </div>
                <div class="mb-1 row">
                  <label for="is_main" class="col-12">
                    Is Main
                  </label>
                  <div class="col-12">
                    <Radio.Group
                      onChange={(e) => {
                        this.setState({
                          is_main: e.target.value,
                        });
                      }}
                      value={this.state.is_main}
                      optionType="button"
                      buttonStyle="solid"
                    >
                      <Radio value={1}>True</Radio>
                      <Radio value={0}>False</Radio>
                    </Radio.Group>
                  </div>
                </div>
                <div class="col-12" style={{ border: "1px solid #ccc" }}>
                  <ReactCrop
                    src={this.state.src[this.state.posisiGbr]}
                    crop={this.state.crop}
                    ruleOfThirds
                    onImageLoaded={(image) => this.onImageLoaded(image)}
                    onComplete={(crop) => this.onCropComplete(crop)}
                    onChange={(crop, percentCrop) =>
                      this.onCropChange(crop, percentCrop)
                    }
                  />
                </div>
              </>
            )}
          </Spin>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = function (state) {
  return state.configReducers;
};

export default connect(mapStateToProps, {
  setConfig,
  ajaxHandler,
  ajaxViewHandler,
})(withRouter(FormArtikel));
