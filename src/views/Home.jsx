import React, { PureComponent } from "react";
import { Redirect, Link } from "react-router-dom";
import { withRouter } from "react-router";
import {
  Button,
  Row,
  Col,
  Input,
  Layout,
  Form,
  Alert,
  Modal,
  message,
  Tabs,
  notification,
} from "antd";
// import "@ant-design/compatible/assets/index.css";
import Config from "../Config";
import { Helmet } from "react-helmet";

import { connect } from "react-redux";
import { setConfig, ajaxHandler, ajaxViewHandler } from "../store/actions";

import axios from "axios";
import Cookies from "js-cookie";

// import Header from "../components/Header.jsx";
// import Menu from "../components/Menu.jsx";
// import Footer from "../components/Footer.jsx";

const configClass = {
  apiUrl: {
    profile: "api/v1.0/user/info/",
  },
};

class Home extends PureComponent {
  config;
  timeoutGetQr;
  timeOutPushQr;

  constructor(props) {
    super(props);
    this.config = new Config();
    this.state = {
      msg: "",
      loading: false,
    };
  }

  componentDidMount() {
    // document.body.classList.remove("blank-page");
  }

  render() {
    return (
      <>
        {/*<!-- BEGIN: Content-->*/}
        <div className="app-content content ">
          <div className="content-overlay"></div>
          <div className="header-navbar-shadow"></div>
          <div className="content-wrapper container-xxl p-0">
            <div className="content-header row"></div>
            <div className="content-body">
              {/*<!-- Dashboard Ecommerce Starts -->*/}
              <section id="dashboard-ecommerce">
                <div className="row match-height">
                  {/*<!-- Medal Card -->*/}
                  <div className="col-xl-4 col-md-6 col-12">
                    <div className="card card-congratulation-medal">
                      <div className="card-body">
                        <h5>Panel 1</h5>
                        <p className="card-text font-small-3">
                          label panel 1
                        </p>
                        
                      </div>
                    </div>
                  </div>
                  {/*<!--/ Medal Card -->*/}

                  {/*<!-- Statistics Card -->*/}
                  <div className="col-xl-8 col-md-6 col-12">
                    <div className="card card-statistics">
                      <div className="card-header">
                        <h4 className="card-title">Panel 2</h4>
                        <div className="d-flex align-items-center">
                          <p className="card-text font-small-2 me-25 mb-0">
                            label panel 2
                          </p>
                        </div>
                      </div>
                      <div className="card-body statistics-body">
                        <div className="row">
                          <div className="col-12">
                            body panel 2
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/*<!--/ Statistics Card -->*/}
                </div>

                

                <div className="row match-height">
                  {/*<!-- Company Table Card -->*/}
                  <div className="col-lg-12 col-12">
                    <div className="card card-company-table">
                      <div className="card-body p-0">
                        <div className="table-responsive">
                          <table className="table">
                            <thead>
                              <tr>
                                <th>Kolom 1</th>
                                <th>Kolom 2</th>
                                <th>Kolom 3</th>
                                <th>Kolom 4</th>
                                <th>Kolom 5</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  baris
                                </td>
                                <td>
                                  baris
                                </td>
                                <td className="text-nowrap">
                                  baris
                                </td>
                                <td>baris</td>
                                <td>
                                  baris
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  baris
                                </td>
                                <td>
                                  baris
                                </td>
                                <td className="text-nowrap">
                                  baris
                                </td>
                                <td>baris</td>
                                <td>
                                  baris
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  baris
                                </td>
                                <td>
                                  baris
                                </td>
                                <td className="text-nowrap">
                                  baris
                                </td>
                                <td>baris</td>
                                <td>
                                  baris
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  baris
                                </td>
                                <td>
                                  baris
                                </td>
                                <td className="text-nowrap">
                                  baris
                                </td>
                                <td>baris</td>
                                <td>
                                  baris
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  baris
                                </td>
                                <td>
                                  baris
                                </td>
                                <td className="text-nowrap">
                                  baris
                                </td>
                                <td>baris</td>
                                <td>
                                  baris
                                </td>
                              </tr>
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/*<!--/ Company Table Card -->*/}
                </div>
              </section>
              {/*<!-- Dashboard Ecommerce ends -->*/}
            </div>
          </div>
        </div>
        {/*<!-- END: Content-->*/}
      </>
    );
  }
}

const mapStateToProps = function (state) {
  return state.configReducers;
};

export default connect(mapStateToProps, {
  setConfig,
  ajaxHandler,
  ajaxViewHandler,
})(withRouter(Home));
