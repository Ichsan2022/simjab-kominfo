import React, { PureComponent } from "react";
import { Redirect, Link } from "react-router-dom";
import { withRouter } from "react-router";
import {
  Button,
  Row,
  Col,
  Input,
  Layout,
  Form,
  Alert,
  Modal,
  message,
  Switch,
  Tabs,
  notification,
  Spin,
  Pagination,
} from "antd";
// import "@ant-design/compatible/assets/index.css";
import Config from "../../Config";
import { Helmet } from "react-helmet";
import moment from "moment";

import { connect } from "react-redux";
import { setConfig, ajaxHandler, ajaxViewHandler } from "../../store/actions";

import axios from "axios";
import Cookies from "js-cookie";

const configClass = {
  apiUrl: {
    preview: "api/cms_portal/category/",
  },
  urlPage: {
    data: "/admin/preferences/kategori-konten",
    name: "Data Kategori",
    form: "Form Kategori",
  }
};

class FormKategori extends PureComponent {
  formRef = React.createRef();
  config;
  timeoutGetQr;
  timeOutPushQr;

  constructor(props) {
    super(props);
    this.config = new Config();
    this.state = {
      errComponent: false,
      errTitle: '',
      errMsg: '',
      loadingData: false,

      aksi: 'tambah',
      method: 'post',
      idLama: '',

      // gambar: null,

      // preview_gambar: "",

      // src: null,
      // crop: {
      //   unit: '%',
      //   width: 100,
      //   aspect: 16 / 16
      // },
      // gambar_jadi: null,
      // type_gambar: null,
    };
  }

  componentDidMount() {
    if (this.props.match.params.id != undefined) {
      this.preview(this.props.match.params.id);
    }
  }

  handleSubmit = (values) =>{
    var datas = new FormData();
    for ( var key in values ) {
      if(values[key] == undefined){
        datas.append(key, '');
      }else{
        datas.append(key, values[key].toString());
      }
    }
    // if(this.state.gambar_jadi != null){
    //   datas.append("icon", this.state.gambar_jadi, this.state.gambar_jadi.name);
    // }
    this.postData(datas);
  }

  async postData(datas) {
    this.setState({
      loadingData: true,
      errTitle: "",
      errMsg: "",
    });
    // message.loading({ content: 'Memproses...', key: 'toast' });

    this.props.ajaxHandler(this.state.method, configClass.apiUrl.preview+this.state.idLama, datas, true, true)
    .then(() =>{
      setTimeout(() =>{
        this.setState({
          loadingData: false
        });
        const data = this.props.responseMsg;
        if(this.props.isSuccess){
          notification.success({
            message: "sukses",
            description: "berhasil menyimpan.",
            placement: "bottomRight",
          });
        }else{
          const errTitle = 'gagal menyimpan data'
          this.setState({
            errTitle: errTitle,
            errMsg: JSON.stringify(this.props.responseMsg)
          });
        }
      }, 500)
    }).catch((response) => {
      this.setState({
        loadingData: false,
        errTitle: "gagal menyimpan",
        errMsg: JSON.stringify(response)
      });
    });
  }

  async preview(id) {
    this.setState({
      loadingData: true,
      errTitle: "",
      errMsg: "",
    });

    this.props
      .ajaxViewHandler("get", configClass.apiUrl.preview + id)
      .then(() => {
        setTimeout(() => {
          const data = this.props.responseMsg.data;
          if (this.props.isSuccess) {
            this.formRef.current.setFieldsValue(data);
            for (var key in data) {
              const dateRegex = /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/;
              const dateTimeRegex =
                /^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}\:[0-9]{2}\:[0-9]{2}$/;
              if (dateRegex.test(data[key])) {
                //jika format tanggal
                this.formRef.current.setFieldsValue({
                  [key + "_picker"]: moment(new Date(data[key]), "DD-MM-YYYY"),
                  [key]: moment(new Date(data[key]), 'DD-MM-YYYY')
                });
              } else if (dateTimeRegex.test(data[key])) {
                //jika format tanggal dan jam
                this.formRef.current.setFieldsValue({
                  [key + "_picker"]: moment(
                    new Date(data[key]),
                    "DD-MM-YYYY HH:mm:ss"
                  ),
                  [key]: moment(new Date(data[key]), 'DD-MM-YYYY HH:mm:ss')
                });
              }else{
                this.formRef.current.setFieldsValue({
                  [key]: data[key]
                });
              }

              // if (key == "icon") {
              //   this.setState({
              //     preview_gambar: data[key],
              //   });
              // }
            }
            message.destroy();
            this.setState({
              aksi: "edit",
              method: "put",
              idLama: id + "/",
              loadingData: false,
            });
          } else {
            const errTitle = "error preview data lama";
            this.setState({
              errTitle: errTitle,
              errMsg: JSON.stringify(data),
              loadingData: false
            });
          }
        }, 500);
      })
      .catch((response) => {
        this.setState({
          errTitle: "gagal get data",
          errMsg: JSON.stringify(response),
          loadingData: false
        })
      });
  }

  render() {
    if(this.props.isRedirect){
      return <Redirect to={configClass.urlPage.data} />
    }

    return (
      <>
        {/*<!-- BEGIN: Content-->*/}
        <div class="app-content content ">
          <div class="content-overlay"></div>
          <div class="header-navbar-shadow"></div>
          <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
              <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                  <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">
                      {configClass.urlPage.form}
                    </h2>
                    <div class="breadcrumb-wrapper">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                          <Link to={"/admin/index"}>
                            <a>Home</a>
                          </Link>
                        </li>
                        <li class="breadcrumb-item">
                          <Link to={configClass.urlPage.data}>
                            <a>{configClass.urlPage.name}</a>
                          </Link>
                        </li>
                        <li class="breadcrumb-item active">{configClass.urlPage.form}</li>
                      </ol>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-body">
              {/*<!-- Table head options start -->*/}
              <div class="row" id="table-head">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">{configClass.urlPage.form}</h4>
                    </div>
                    <div class="card-body">
                      {this.state.errMsg != "" &&
                        
                          <div className="alert alert-danger alert-dismissible fade show" role="alert">
                            <div className="alert-body d-flex align-items-center">
                              {this.state.errMsg}
                            </div>
                          </div>
                        
                      }
                      <Spin tip="Loading..." spinning={this.state.loadingData}>
                        <Form 
                          // layout="horizontal"
                          onFinish={this.handleSubmit}
                          size="small"
                          ref={this.formRef}
                          size="default"
                        > 
                          <div class="mb-1 row">
                            <label for="name" class="col-12">Nama</label>
                            <div class="col-12">
                              <Form.Item
                                name="name"
                                rules={[
                                  { 
                                    required: true, 
                                    message: 'harus diisi' 
                                  }
                                ]}
                              >
                                <Input type="text" id="name" class="form-control" placeholder="input nama kategori" />
                              </Form.Item>
                            </div>
                          </div>
                          <button class="btn btn-primary" type="submit">Submit</button>

                        </Form>                       
                      </Spin>
                    </div>
                  </div>
                </div>
              </div>
              {/*<!-- Table head options end -->*/}
            </div>
          </div>
        </div>
        {/*<!-- END: Content-->*/}
      </>
    );
  }
}

const mapStateToProps = function (state) {
  return state.configReducers;
};

export default connect(mapStateToProps, {
  setConfig,
  ajaxHandler,
  ajaxViewHandler,
})(withRouter(FormKategori));
