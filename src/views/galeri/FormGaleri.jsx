import React, { PureComponent } from "react";
import { Redirect, Link } from "react-router-dom";
import { withRouter } from "react-router";
import {
  Button,
  Row,
  Col,
  Input,
  Layout,
  Form,
  Alert,
  Modal,
  message,
  Switch,
  Tabs,
  notification,
  Spin,
  Pagination,
  Radio,
  Select,
} from "antd";
import {
  TeamOutlined,
  SearchOutlined,
  PlusOutlined,
  FormOutlined,
  CloseSquareOutlined,
  ExclamationCircleOutlined,
  DiffOutlined,
  RedoOutlined,
} from "@ant-design/icons";
// import "@ant-design/compatible/assets/index.css";
import Config from "../../Config";
import { Helmet } from "react-helmet";
import moment from "moment";

import { connect } from "react-redux";
import { setConfig, ajaxHandler, ajaxViewHandler } from "../../store/actions";
import { Editor } from "@tinymce/tinymce-react";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import axios from "axios";
import Cookies from "js-cookie";
import slugify from "react-slugify";

const configClass = {
  apiUrl: {
    preview: "api/cms_portal/content/image/",
  },
  urlPage: {
    data: "/admin/cms/galeri",
    name: "Data Galeri",
    form: "Form Galeri",
  },
  limit: 20,
};

class FormGaleri extends PureComponent {
  formRef = React.createRef();
  config;
  timeoutGetQr;
  timeOutPushQr;

  constructor(props) {
    super(props);
    this.config = new Config();
    this.state = {
      errComponent: false,
      errTitle: "",
      errMsg: "",
      loadingData: false,

      aksi: "tambah",
      method: "post",
      idLama: "",

      preview_gambar: null,
      src: null,
      name_gambar: null,
      type_gambar: null,
      gambar_jadi: null,
      croppedImageUrl: null,
      crop: {
        unit: "%",
        width: 100,
        aspect: 16 / 9,
      },

      isSukses: false,
    };
  }

  componentDidMount() {
    if (this.props.match.params.id != undefined) {
      this.preview(this.props.match.params.id);
    }
  }

  onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        this.setState({ src: reader.result });
      });
      reader.readAsDataURL(e.target.files[0]);

      this.setState({
        type_gambar: e.target.files[0].type,
        name_gambar: e.target.files[0].name,
      });
    }
  };

  onImageLoaded = (image) => {
    this.imageRef = image;
  };

  onCropComplete = (crop) => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop) => {
    // You could also use percentCrop:
    // this.setState({ crop: percentCrop });
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        this.state.name_gambar
      );

      // let tempcroppedImageUrl = this.state.croppedImageUrl;
      // tempcroppedImageUrl = [...tempcroppedImageUrl, croppedImageUrl];
      // this.setState({ croppedImageUrl: tempcroppedImageUrl });
    }
  }

  getCroppedImg(image, crop, fileName) {
    var canvas = document.createElement("canvas");
    canvas.setAttribute("id", "gbr-jadi");
    const pixelRatio = window.devicePixelRatio;
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext("2d");

    canvas.width = crop.width * pixelRatio * scaleX;
    canvas.height = crop.height * pixelRatio * scaleY;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = "high";

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width * scaleX,
      crop.height * scaleY
    );

    var that = this;

    return new Promise((resolve, reject) => {
      canvas.toBlob(
        (blob) => {
          if (!blob) {
            //reject(new Error('Canvas is empty'));
            console.error("Canvas is empty");
            return;
          }

          blob.name = fileName;
          window.URL.revokeObjectURL(this.fileUrl);
          this.fileUrl = window.URL.createObjectURL(blob);

          that.setState({
            gambar_jadi: blob,
            croppedImageUrl: this.fileUrl,
          });
          resolve(this.fileUrl);
        },
        that.state.type_gambar,
        1
      );
    });
  }

  handleSubmit = (values) => {
    var datas = new FormData();
    for (var key in values) {
      if (values[key] == undefined) {
        datas.append(key, "");
      } else {
        datas.append(key, values[key].toString());
      }
    }
    // datas.append('provider', this.props.id);
    if (this.state.gambar_jadi != null) {
      datas.append(
        "image",
        this.state.gambar_jadi,
        this.state.gambar_jadi.name
      );
    }

    this.postData(datas);
  };

  async postData(datas) {
    this.setState({
      loadingData: true,
      errTitle: "",
      errMsg: "",
    });
    // message.loading({ content: 'Memproses...', key: 'toast' });

    this.props
      .ajaxHandler(
        this.state.method,
        configClass.apiUrl.preview + this.state.idLama,
        datas,
        true,
        true
      )
      .then(() => {
        setTimeout(() => {
          this.setState({
            loadingData: false,
          });
          const data = this.props.responseMsg;
          if (this.props.isSuccess) {
            notification.success({
              message: "sukses",
              description: "berhasil menyimpan.",
              placement: "bottomRight",
            });
            this.setState({
              isSukses: true,
            });
          } else {
            const errTitle = "gagal menyimpan data";
            this.setState({
              errTitle: errTitle,
              errMsg: JSON.stringify(this.props.responseMsg),
            });
          }
        }, 500);
      })
      .catch((response) => {
        this.setState({
          loadingData: false,
          errTitle: "gagal menyimpan",
          errMsg: JSON.stringify(response),
        });
      });
  }

  async preview(id) {
    this.setState({
      loadingData: true,
      errTitle: "",
      errMsg: "",
    });

    this.props
      .ajaxViewHandler("get", configClass.apiUrl.preview + id)
      .then(() => {
        setTimeout(() => {
          const data = this.props.responseMsg.data;
          if (this.props.isSuccess) {
            this.formRef.current.setFieldsValue(data);
            for (var key in data) {
              const dateRegex = /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/;
              const dateTimeRegex =
                /^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}\:[0-9]{2}\:[0-9]{2}$/;
              if (dateRegex.test(data[key])) {
                //jika format tanggal
                this.formRef.current.setFieldsValue({
                  [key + "_picker"]: moment(new Date(data[key]), "DD-MM-YYYY"),
                  [key]: moment(new Date(data[key]), "DD-MM-YYYY"),
                });
              } else if (dateTimeRegex.test(data[key])) {
                //jika format tanggal dan jam
                this.formRef.current.setFieldsValue({
                  [key + "_picker"]: moment(
                    new Date(data[key]),
                    "DD-MM-YYYY HH:mm:ss"
                  ),
                  [key]: moment(new Date(data[key]), "DD-MM-YYYY HH:mm:ss"),
                });
              } else {
                if (key == "image") {
                  this.setState({
                    preview_gambar: data[key],
                  });
                } else {
                  this.formRef.current.setFieldsValue({
                    [key]: data[key],
                  });
                }
              }

              // if (key == "icon") {
              //   this.setState({
              //     preview_gambar: data[key],
              //   });
              // }
            }
            message.destroy();
            this.setState({
              aksi: "edit",
              method: "put",
              idLama: data.id + "/",
              loadingData: false,
            });
          } else {
            const errTitle = "error preview data lama";
            this.setState({
              errTitle: errTitle,
              errMsg: JSON.stringify(data),
              loadingData: false,
            });
          }
        }, 500);
      })
      .catch((response) => {
        this.setState({
          errTitle: "gagal get data",
          errMsg: JSON.stringify(response),
          loadingData: false,
        });
      });
  }

  render() {
    if (this.state.isSukses) {
      return <Redirect to={configClass.urlPage.data} />;
    }

    // const { crop, croppedImageUrl, src } = this.state;

    return (
      <>
        {/*<!-- BEGIN: Content-->*/}
        <div class="app-content content ">
          <div class="content-overlay"></div>
          <div class="header-navbar-shadow"></div>
          <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
              <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                  <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">
                      {configClass.urlPage.form}
                    </h2>
                    <div class="breadcrumb-wrapper">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                          <Link to={"/admin/index"}>
                            <a>Home</a>
                          </Link>
                        </li>
                        <li class="breadcrumb-item">
                          <Link to={configClass.urlPage.data}>
                            <a>{configClass.urlPage.name}</a>
                          </Link>
                        </li>
                        <li class="breadcrumb-item active">
                          {configClass.urlPage.form}
                        </li>
                      </ol>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-body">
              {/*<!-- Table head options start -->*/}
              <div class="row" id="table-head">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">{configClass.urlPage.form}</h4>
                    </div>
                    <div class="card-body">
                      {this.state.errMsg != "" && (
                        <div
                          className="alert alert-danger alert-dismissible fade show"
                          role="alert"
                        >
                          <div className="alert-body d-flex align-items-center">
                            {this.state.errMsg}
                          </div>
                        </div>
                      )}
                      <Spin tip="Loading..." spinning={this.state.loadingData}>
                        <Form
                          // layout="horizontal"
                          onFinish={this.handleSubmit}
                          size="small"
                          ref={this.formRef}
                          size="default"
                          initialValues={{
                            is_banner: 0,
                            is_main: 0,
                          }}
                        >
                          <div class="mb-1 row">
                            <label for="name" class="col-12">
                              Name
                            </label>
                            <div class="col-12">
                              <Form.Item
                                name="name"
                                rules={[
                                  {
                                    required: true,
                                    message: "harus diisi",
                                  },
                                ]}
                              >
                                <Input
                                  type="text"
                                  id="name"
                                  class="form-control"
                                  placeholder="input title"
                                />
                              </Form.Item>
                            </div>
                          </div>
                          <div class="mb-1 row">
                            <label for="is_banner" class="col-12">
                              Is Banner
                            </label>
                            <div class="col-12">
                              <Form.Item
                                name="is_banner"
                                rules={[
                                  {
                                    required: true,
                                    message: "harus dipilih",
                                  },
                                ]}
                              >
                                <Radio.Group
                                  // onChange={(e) => {
                                  //   this.setState({
                                  //     is_banner: e.target.value,
                                  //   });
                                  // }}
                                  // defaultValue={this.state.is_banner}
                                  // value={this.state.is_banner}
                                  optionType="button"
                                  buttonStyle="solid"
                                >
                                  <Radio value={1}>True</Radio>
                                  <Radio value={0}>False</Radio>
                                </Radio.Group>
                              </Form.Item>
                            </div>
                          </div>
                          <div class="mb-1 row">
                            <label for="is_main" class="col-12">
                              Is Main
                            </label>
                            <div class="col-12">
                              <Form.Item
                                name="is_main"
                                rules={[
                                  {
                                    required: true,
                                    message: "harus dipilih",
                                  },
                                ]}
                              >
                                <Radio.Group
                                  // onChange={(e) => {
                                  //   this.setState({
                                  //     is_main: e.target.value,
                                  //   });
                                  // }}
                                  // value={this.state.is_main}
                                  optionType="button"
                                  buttonStyle="solid"
                                >
                                  <Radio value={1}>True</Radio>
                                  <Radio value={0}>False</Radio>
                                </Radio.Group>
                              </Form.Item>
                            </div>
                          </div>
                          <div class="mb-1 row">
                            <label for="name" class="col-12">
                              Gambar
                            </label>
                            <div class="col-12">
                              {this.state.aksi == "edit" &&
                                this.state.preview_gambar != null && (
                                  <div
                                    key={"gbredit"}
                                    className="d-inline-block"
                                    style={{ marginRight: 5 }}
                                  >
                                    <div
                                      className="add-image"
                                      style={{
                                        position: "relative",
                                      }}
                                    >
                                      <img
                                        width={100}
                                        src={this.sate.preview_gambar}
                                      />
                                      />
                                    </div>
                                  </div>
                                )}

                              <div
                                key={"gbr"}
                                className="d-inline-block"
                                style={{ marginRight: 5 }}
                              >
                                <div className="add-image">
                                  <img
                                    id={"pilihGbr"}
                                    width={100}
                                    src={
                                      process.env.PUBLIC_URL + "/add_image.png"
                                    }
                                    onClick={(e) => {
                                      document.getElementById("upload").click();
                                    }}
                                    title={"pilih gambar"}
                                  />
                                </div>
                                <Input
                                  style={{ display: "none" }}
                                  id={"upload"}
                                  accept="image/png, image/jpeg"
                                  type="file"
                                  placeholder="pilih gambar"
                                  // onChange={this.setGambar}
                                  onChange={(e) => this.onSelectFile(e)}
                                />
                              </div>
                            </div>
                          </div>
                          <div
                            class="col-12"
                            style={{ border: "1px solid #ccc" }}
                          >
                            <ReactCrop
                              src={this.state.src}
                              crop={this.state.crop}
                              ruleOfThirds
                              onImageLoaded={(image) =>
                                this.onImageLoaded(image)
                              }
                              onComplete={(crop) => this.onCropComplete(crop)}
                              onChange={(crop, percentCrop) =>
                                this.onCropChange(crop, percentCrop)
                              }
                            />
                          </div>

                          <button class="btn btn-primary" type="submit">
                            Submit
                          </button>
                          <Link to={configClass.urlPage.data}>
                            <button
                              class="btn btn-danger"
                              type="button"
                              style={{ marginLeft: 10 }}
                            >
                              Cancel
                            </button>
                          </Link>
                        </Form>
                      </Spin>
                    </div>
                  </div>
                </div>
              </div>
              {/*<!-- Table head options end -->*/}
            </div>
          </div>
        </div>
        {/*<!-- END: Content-->*/}
      </>
    );
  }
}

const mapStateToProps = function (state) {
  return state.configReducers;
};

export default connect(mapStateToProps, {
  setConfig,
  ajaxHandler,
  ajaxViewHandler,
})(withRouter(FormGaleri));
