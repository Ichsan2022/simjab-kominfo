import React, { PureComponent } from "react";
import { Redirect, Link } from "react-router-dom";
import { withRouter } from "react-router";
import {
  Button,
  Row,
  Col,
  Input,
  Layout,
  Form,
  Alert,
  Modal,
  message,
  Switch,
  Tabs,
  notification,
} from "antd";
// import "@ant-design/compatible/assets/index.css";
import Config from "../Config";
import { Helmet } from "react-helmet";

import { connect } from "react-redux";
import { setConfig, ajaxHandler, ajaxViewHandler } from "../store/actions";

import axios from "axios";
import Cookies from "js-cookie";

const configClass = {
  apiUrl: {
    profile: "api/v1.0/user/info/",
  },
};

class NotFound extends PureComponent {
  config;
  timeoutGetQr;
  timeOutPushQr;

  constructor(props) {
    super(props);
    this.config = new Config();
    this.state = {
      msg: "",
      loading: false,
    };
  }

  componentDidMount() {
    document.body.classList.remove("blank-page");
  }

  render() {
    return (
      <>
        {/*<!-- BEGIN: Content-->*/}
        <div class="app-content content ">
          <div class="content-overlay"></div>
          <div class="header-navbar-shadow"></div>
          <div class="content-wrapper container-xxl p-0">
            <div class="content-header row">
              <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                  <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">
                      Page Not Found
                    </h2>
                    <div class="breadcrumb-wrapper">
                      <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                          <a href="index.html">Home</a>
                        </li>
                        <li class="breadcrumb-item active">Page Not Found</li>
                      </ol>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="content-body">
              <div class="row">
                    <div class="col-12">
                        <div class="alert alert-warning" role="alert">
                            <div class="alert-body">
                                <strong>Warning:</strong> maaf halaman yang anda tuju tidak ditemukan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
        {/*<!-- END: Content-->*/}
      </>
    );
  }
}

const mapStateToProps = function (state) {
  return state.configReducers;
};

export default connect(mapStateToProps, {
  setConfig,
  ajaxHandler,
  ajaxViewHandler,
})(withRouter(NotFound));
