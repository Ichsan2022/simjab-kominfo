import React, { PureComponent } from "react";
import { Redirect, Link } from "react-router-dom";
import { withRouter } from "react-router";
import {
  Button,
  Row,
  Col,
  Input,
  Layout,
  Form,
  Alert,
  Modal,
  message,
  Switch,
  Tabs,
  notification,
} from "antd";
// import "@ant-design/compatible/assets/index.css";
import Config from "../Config";
import { Helmet } from "react-helmet";

import { connect } from "react-redux";
import { setConfig, ajaxHandler, ajaxViewHandler } from "../store/actions";

import axios from "axios";
import Cookies from "js-cookie";

const configClass = {
  apiUrl: {
    profile: "api/v1.0/user/info/",
  },
};

class Footer extends PureComponent {
  config;
  timeoutGetQr;
  timeOutPushQr;

  constructor(props) {
    super(props);
    this.config = new Config();
    this.state = {
      msg: "",
      loading: false,
    };
  }

  componentDidMount() {
  	// document.body.classList.remove('blank-page'); 
  }

  render() {
    return (
      <>

        {/*<!-- BEGIN: Footer-->*/}
        <footer className="footer footer-static footer-light">
          <p className="clearfix mb-0">
            <span className="float-md-start d-block d-md-inline-block mt-25">
              COPYRIGHT &copy; 2022
              <a
                className="ms-25"
                href="/"
                target="_blank"
              >
                Kominfo
              </a>
              <span className="d-none d-sm-inline-block">
                , All rights Reserved
              </span>
            </span>
            
          </p>
        </footer>
        <button className="btn btn-primary btn-icon scroll-top" type="button" onClick={() => window.scrollTo(0, 0)}>
          <i data-feather="arrow-up"></i>
        </button>
        {/*<!-- END: Footer-->*/}
      </>
    );
  }
}

const mapStateToProps = function (state) {
  return state.configReducers;
};

export default connect(mapStateToProps, {
  setConfig,
  ajaxHandler,
  ajaxViewHandler,
})(withRouter(Footer));
