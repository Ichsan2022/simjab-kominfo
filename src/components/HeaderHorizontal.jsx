import React, { PureComponent } from "react";
import { Redirect, Link } from "react-router-dom";
import { withRouter } from "react-router";
import {
  Button,
  Row,
  Col,
  Input,
  Layout,
  Form,
  Alert,
  Modal,
  message,
  Switch,
  Tabs,
  notification,
} from "antd";
// import "@ant-design/compatible/assets/index.css";
import Config from "../Config";
import { Helmet } from "react-helmet";

import { connect } from "react-redux";
import { setConfig, ajaxHandler, ajaxViewHandler } from "../store/actions";

import axios from "axios";
import Cookies from "js-cookie";

const configClass = {
  apiUrl: {
    profile: "api/v1.0/user/info/",
  },
};

class Header extends PureComponent {
  config;
  timeoutGetQr;
  timeOutPushQr;

  constructor(props) {
    super(props);
    this.config = new Config();
    this.state = {
      msg: "",
      loading: false,
    };
  }

  componentDidMount() {
    // document.body.classList.remove("blank-page");
  }

  prosesLogout = (e) =>{
    e.preventDefault();
      Cookies.remove(this.config.get_cookie_name());
      //Cookies.remove('aBdi_d3s4uth_temp');
      this.props.setConfig({
        isLogin: false
      });
  };

  render() {
    return (
      <>
        {/*<!-- BEGIN: Header-->*/}
        <nav
          className="header-navbar navbar-expand-lg navbar navbar-fixed align-items-center navbar-shadow navbar-brand-center"
          data-nav="brand-center"
          // style={{
          //   background: "inherit",
          //   boxShadow: "inherit"
          // }}
        >
          <div className="navbar-header d-xl-block d-none">
            <ul className="nav navbar-nav">
              <li className="nav-item">
                <a className="navbar-brand" href={"/"}>
                  <span className="brand-logo">
                    <img 
                      src={
                        process.env.PUBLIC_URL + "/kominfo.jpg"
                      } 
                    />
                  </span>
                  <h2 className="brand-text mb-0">CMS Website</h2>
                </a>
              </li>
            </ul>
          </div>
          <div className="navbar-container d-flex content">
            <div className="bookmark-wrapper d-flex align-items-center">
              <ul className="nav navbar-nav d-xl-none">
                <li className="nav-item">
                  <a className="nav-link menu-toggle" href="#">
                    <i className="ficon" data-feather="menu"></i>
                  </a>
                </li>
              </ul>
              {/*<ul className="nav navbar-nav bookmark-icons">
                <li className="nav-item d-none d-lg-block">
                  <a  href={"/"}>
                    <span className="brand-logo d-inline">
                      <img 
                        src={
                          process.env.PUBLIC_URL + "/kominfo.jpg"
                        } 
                        height={30}
                      />
                    </span>
                  </a>
                </li>
                <li className="nav-item d-none d-lg-block">
                  <h2 className="brand-text mb-0">CMS Website</h2>
                </li>
              </ul>*/}
            </div>
            <ul className="nav navbar-nav align-items-center ms-auto">
              <li className="nav-item dropdown dropdown-user">
                <a
                  className="nav-link dropdown-toggle dropdown-user-link"
                  id="dropdown-user"
                  href="#"
                  data-bs-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  <div className="user-nav d-sm-flex d-none">
                    <span className="user-name fw-bolder">{Cookies.get(this.config.get_cookie_name()+"_nama")}</span>
                    <span className="user-status">Admin</span>
                  </div>
                  <span className="avatar">
                    <img
                      className="round"
                      src={
                        process.env.PUBLIC_URL +
                        "/app-assets//images/portrait/small/avatar-s-11.jpg"
                      }
                      alt="avatar"
                      height="40"
                      width="40"
                    />
                    <span className="avatar-status-online"></span>
                  </span>
                </a>
                <div
                  className="dropdown-menu dropdown-menu-end"
                  aria-labelledby="dropdown-user"
                >
                  <a className="dropdown-item" href="page-profile.html">
                    <i className="me-50" data-feather="user"></i> Profile
                  </a>
                  <div className="dropdown-divider"></div>
                  <a className="dropdown-item" href="#" onClick={(e) => this.prosesLogout(e)}>
                    <i className="me-50" data-feather="power"></i> Logout
                  </a>
                </div>
              </li>
            </ul>
          </div>
        </nav>
        <ul className="main-search-list-defaultlist d-none">
          <li className="d-flex align-items-center">
            <a href="#">
              <h6 className="section-label mt-75 mb-0">Files</h6>
            </a>
          </li>
          <li className="auto-suggestion">
            <a
              className="d-flex align-items-center justify-content-between w-100"
              href="app-file-manager.html"
            >
              <div className="d-flex">
                <div className="me-75">
                  <img
                    src={
                      process.env.PUBLIC_URL +
                      "/app-assets/images/icons/xls.png"
                    }
                    alt="png"
                    height="32"
                  />
                </div>
                <div className="search-data">
                  <p className="search-data-title mb-0">
                    Two new item submitted
                  </p>
                  <small className="text-muted">Marketing Manager</small>
                </div>
              </div>
              <small className="search-data-size me-50 text-muted">
                &apos;17kb
              </small>
            </a>
          </li>
          <li className="auto-suggestion">
            <a
              className="d-flex align-items-center justify-content-between w-100"
              href="app-file-manager.html"
            >
              <div className="d-flex">
                <div className="me-75">
                  <img
                    src={
                      process.env.PUBLIC_URL +
                      "/app-assets/images/icons/jpg.png"
                    }
                    alt="png"
                    height="32"
                  />
                </div>
                <div className="search-data">
                  <p className="search-data-title mb-0">
                    52 JPG file Generated
                  </p>
                  <small className="text-muted">FontEnd Developer</small>
                </div>
              </div>
              <small className="search-data-size me-50 text-muted">
                &apos;11kb
              </small>
            </a>
          </li>
          <li className="auto-suggestion">
            <a
              className="d-flex align-items-center justify-content-between w-100"
              href="app-file-manager.html"
            >
              <div className="d-flex">
                <div className="me-75">
                  <img
                    src={
                      process.env.PUBLIC_URL +
                      "/app-assets/images/icons/pdf.png"
                    }
                    alt="png"
                    height="32"
                  />
                </div>
                <div className="search-data">
                  <p className="search-data-title mb-0">25 PDF File Uploaded</p>
                  <small className="text-muted">
                    Digital Marketing Manager
                  </small>
                </div>
              </div>
              <small className="search-data-size me-50 text-muted">
                &apos;150kb
              </small>
            </a>
          </li>
          <li className="auto-suggestion">
            <a
              className="d-flex align-items-center justify-content-between w-100"
              href="app-file-manager.html"
            >
              <div className="d-flex">
                <div className="me-75">
                  <img
                    src={
                      process.env.PUBLIC_URL +
                      "/app-assets/images/icons/doc.png"
                    }
                    alt="png"
                    height="32"
                  />
                </div>
                <div className="search-data">
                  <p className="search-data-title mb-0">Anna_Strong.doc</p>
                  <small className="text-muted">Web Designer</small>
                </div>
              </div>
              <small className="search-data-size me-50 text-muted">
                &apos;256kb
              </small>
            </a>
          </li>
          <li className="d-flex align-items-center">
            <a href="#">
              <h6 className="section-label mt-75 mb-0">Members</h6>
            </a>
          </li>
          <li className="auto-suggestion">
            <a
              className="d-flex align-items-center justify-content-between py-50 w-100"
              href="app-user-view-account.html"
            >
              <div className="d-flex align-items-center">
                <div className="avatar me-75">
                  <img
                    src={
                      process.env.PUBLIC_URL +
                      "/app-assets/images/portrait/small/avatar-s-8.jpg"
                    }
                    alt="png"
                    height="32"
                  />
                </div>
                <div className="search-data">
                  <p className="search-data-title mb-0">John Doe</p>
                  <small className="text-muted">UI designer</small>
                </div>
              </div>
            </a>
          </li>
          <li className="auto-suggestion">
            <a
              className="d-flex align-items-center justify-content-between py-50 w-100"
              href="app-user-view-account.html"
            >
              <div className="d-flex align-items-center">
                <div className="avatar me-75">
                  <img
                    src={
                      process.env.PUBLIC_URL +
                      "/app-assets/images/portrait/small/avatar-s-1.jpg"
                    }
                    alt="png"
                    height="32"
                  />
                </div>
                <div className="search-data">
                  <p className="search-data-title mb-0">Michal Clark</p>
                  <small className="text-muted">FontEnd Developer</small>
                </div>
              </div>
            </a>
          </li>
          <li className="auto-suggestion">
            <a
              className="d-flex align-items-center justify-content-between py-50 w-100"
              href="app-user-view-account.html"
            >
              <div className="d-flex align-items-center">
                <div className="avatar me-75">
                  <img
                    src={
                      process.env.PUBLIC_URL +
                      "/app-assets/images/portrait/small/avatar-s-14.jpg"
                    }
                    alt="png"
                    height="32"
                  />
                </div>
                <div className="search-data">
                  <p className="search-data-title mb-0">Milena Gibson</p>
                  <small className="text-muted">
                    Digital Marketing Manager
                  </small>
                </div>
              </div>
            </a>
          </li>
          <li className="auto-suggestion">
            <a
              className="d-flex align-items-center justify-content-between py-50 w-100"
              href="app-user-view-account.html"
            >
              <div className="d-flex align-items-center">
                <div className="avatar me-75">
                  <img
                    src={
                      process.env.PUBLIC_URL +
                      "/app-assets/images/portrait/small/avatar-s-6.jpg"
                    }
                    alt="png"
                    height="32"
                  />
                </div>
                <div className="search-data">
                  <p className="search-data-title mb-0">Anna Strong</p>
                  <small className="text-muted">Web Designer</small>
                </div>
              </div>
            </a>
          </li>
        </ul>
        <ul className="main-search-list-defaultlist-other-list d-none">
          <li className="auto-suggestion justify-content-between">
            <a className="d-flex align-items-center justify-content-between w-100 py-50">
              <div className="d-flex justify-content-start">
                <span className="me-75" data-feather="alert-circle"></span>
                <span>No results found.</span>
              </div>
            </a>
          </li>
        </ul>
        {/*<!-- END: Header-->*/}
      </>
    );
  }
}

const mapStateToProps = function (state) {
  return state.configReducers;
};

export default connect(mapStateToProps, {
  setConfig,
  ajaxHandler,
  ajaxViewHandler,
})(withRouter(Header));
