import React, { PureComponent } from "react";
import { Redirect, Link, withRouter } from "react-router-dom";
// import { withRouter } from "react-router";
import {
  Button,
  Row,
  Col,
  Input,
  Layout,
  Form,
  Alert,
  Modal,
  message,
  Switch,
  Tabs,
  notification,
} from "antd";
// import "@ant-design/compatible/assets/index.css";
import Config from "../Config";
import { Helmet } from "react-helmet";

import { connect } from "react-redux";
import { setConfig, ajaxHandler, ajaxViewHandler } from "../store/actions";

import axios from "axios";
import Cookies from "js-cookie";

const configClass = {
  apiUrl: {
    profile: "api/v1.0/user/info/",
  },
};


class MenuHorizontal extends PureComponent {
  config;
  timeoutGetQr;
  timeOutPushQr;

  constructor(props) {
    super(props);
    this.config = new Config();
    this.state = {
      msg: "",
      loading: false,
    };
  }

  // loadScript = (url) => {
  //   const script = document.createElement('script');
  //   script.src = url;
  //   document.body.appendChild(script);
  //   // script.onload = () => { 
  //   //   if (callback) callback();
  //   // };
  // }

  refreshPage = () =>{
    window.location.reload();
  };

  componentWillUnmount() {
    window.removeEventListener('resize', this.refreshPage);
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    // alert(JSON.stringify(this.props.history.location))
    // if(this.props.history.location != undefined && this.props.history.location.state.from == "/auth"){

    // }

    window.addEventListener('resize', this.refreshPage);
    setTimeout(() =>{
      // this.loadScript(process.env.PUBLIC_URL + "/app-assets/vendors/js/vendors.min.js")
      // this.loadScript(process.env.PUBLIC_URL + "/app-assets/vendors/js/ui/jquery.sticky.js")
      // this.loadScript(process.env.PUBLIC_URL + "/app-assets/vendors/js/extensions/toastr.min.js")
      // this.loadScript(process.env.PUBLIC_URL + "/app-assets/js/core/app-menu.js")
      // this.loadScript(process.env.PUBLIC_URL + "/app-assets/js/core/app.js")
    }, 1000)
    // loadScript(process.env.PUBLIC_URL + "/app-assets/js/core/app-menu.js")
    // loadScript(process.env.PUBLIC_URL + "/app-assets/js/core/app-menu.js")
    // document.body.classList.remove("blank-page");

    // var that = this;
    // document.onclick = function (e) {
    //   e = e || window.event;
    //   var element = e.target || e.srcElement;
    //   document
    //     .getElementById("kategori")
    //     .addEventListener("click", function (event) {
    //       that.props.history.push("/admin/kategori");
    //     });
    //   // if (e.tagName == 'A' && element.getAttribute("destinasi")!=undefined) {
    //   //   alert('oke')
    //   // }
    // };

    var that = this;
    document.onclick = function (e) {
      e = e ||  window.event;
      var element = e.target || e.srcElement;
      document.getElementById("home").addEventListener('click', function(event){
        that.props.history.push("/admin/index")
      });
      document.getElementById("pengumuman").addEventListener('click', function(event){
        that.props.history.push("/admin/cms/pengumuman")
      });
      document.getElementById("artikel").addEventListener('click', function(event){
        that.props.history.push("/admin/cms/artikel")
      });
      document.getElementById("pengumuman").addEventListener('click', function(event){
        that.props.history.push("/admin/cms/pengumuman")
      });
      document.getElementById("regulasi").addEventListener('click', function(event){
        that.props.history.push("/admin/cms/regulasi-asn")
      });
      document.getElementById("galeri").addEventListener('click', function(event){
        that.props.history.push("/admin/cms/galeri")
      });
      document.getElementById("users").addEventListener('click', function(event){
        that.props.history.push("/admin/settings/user")
      });

      // document.getElementById("kategori").addEventListener('click', function(event){
      //   that.props.history.push("/admin/preferences/kategori-konten")
      // });
      // if (e.tagName == 'A' && element.getAttribute("destinasi")!=undefined) {
      //   alert('oke')
      // }
    }
  }

  goTo(href) {
    alert(href);
  }

  render() {
    return (
      <>
        {/*<!-- BEGIN: Main Menu-->*/}
        <div className="horizontal-menu-wrapper">
          <div
            className="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-light navbar-shadow menu-border container-xxl"
            role="navigation"
            data-menu="menu-wrapper"
            data-menu-type="floating-nav"
          >
            <div className="navbar-header">
              <ul className="nav navbar-nav flex-row">
                <li className="nav-item me-auto">
                  <Link href="/">
                    <a
                      className="navbar-brand"
                    >
                      <span className="brand-logo">
                        <img src={this.config.get_site_info().logo} />
                        {/*<svg
                          viewBox="0 0 139 95"
                          version="1.1"
                          xmlns="http://www.w3.org/2000/svg"
                          xmlnsXlink="http://www.w3.org/1999/xlink"
                          height="24"
                        >
                          <defs>
                            <lineargradient
                              id="linearGradient-1"
                              x1="100%"
                              y1="10.5120544%"
                              x2="50%"
                              y2="89.4879456%"
                            >
                              <stop stopColor="#000000" offset="0%"></stop>
                              <stop stopColor="#FFFFFF" offset="100%"></stop>
                            </lineargradient>
                            <lineargradient
                              id="linearGradient-2"
                              x1="64.0437835%"
                              y1="46.3276743%"
                              x2="37.373316%"
                              y2="100%"
                            >
                              <stop
                                stopColor="#EEEEEE"
                                stopOpacity="0"
                                offset="0%"
                              ></stop>
                              <stop stopColor="#FFFFFF" offset="100%"></stop>
                            </lineargradient>
                          </defs>
                          <g
                            id="Page-1"
                            stroke="none"
                            strokeWidth="1"
                            fill="none"
                            fillRule="evenodd"
                          >
                            <g
                              id="Artboard"
                              transform="translate(-400.000000, -178.000000)"
                            >
                              <g
                                id="Group"
                                transform="translate(400.000000, 178.000000)"
                              >
                                <path
                                  className="text-primary"
                                  id="Path"
                                  d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z"
                                  style={{fill: "currentColor"}}
                                ></path>
                                <path
                                  id="Path1"
                                  d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z"
                                  fill="url(#linearGradient-1)"
                                  opacity="0.2"
                                ></path>
                                <polygon
                                  id="Path-2"
                                  fill="#000000"
                                  opacity="0.049999997"
                                  points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"
                                ></polygon>
                                <polygon
                                  id="Path-21"
                                  fill="#000000"
                                  opacity="0.099999994"
                                  points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"
                                ></polygon>
                                <polygon
                                  id="Path-3"
                                  fill="url(#linearGradient-2)"
                                  opacity="0.099999994"
                                  points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"
                                ></polygon>
                              </g>
                            </g>
                          </g>
                        </svg>*/}
                      </span>
                      <h2 className="brand-text mb-0">{this.config.get_site_info().name}</h2>
                    </a>
                  </Link>
                </li>
                <li className="nav-item nav-toggle">
                  <a
                    className="nav-link modern-nav-toggle pe-0"
                    data-bs-toggle="collapse"
                  >
                    <i
                      className="d-block d-xl-none text-primary toggle-icon font-medium-4"
                      data-feather="x"
                    ></i>
                  </a>
                </li>
              </ul>
            </div>
            <div className="shadow-bottom"></div>
            {/*<!-- Horizontal menu content-->*/}
            <div
              className="navbar-container main-menu-content"
              data-menu="menu-container"
            >
              {/*<!-- include ../../../includes/mixins-->*/}
              <ul
                className="nav navbar-nav"
                id="main-menu-navigation"
                data-menu="menu-navigation"
              >
                <li className="nav-item">
                  <a
                   className="nav-link d-flex align-items-center"
                   id="home"
                  >
                      <i data-feather="home"></i>
                      <span data-i18n="Home">Home</span>
                  </a>
                </li>
                
                <li className="dropdown nav-item" data-menu="dropdown">
                  <a
                    className="dropdown-toggle nav-link d-flex align-items-center"
                    data-bs-toggle="dropdown"
                  >
                    <i data-feather="edit"></i>
                    <span data-i18n="CMS">CMS</span>
                  </a>
                  <ul className="dropdown-menu" data-bs-popper="none">
                    <li data-menu="" className="">
                      <a
                        className="dropdown-item d-flex align-items-center"
                        data-bs-toggle=""
                        data-i18n="Pengumuman"
                        id="pengumuman"
                      >
                        <i data-feather="file-text"></i>
                        <span data-i18n="Pengumuman">Pengumuman</span>
                      </a>
                    </li>
                    <li data-menu="" className="">
                      <a
                        className="dropdown-item d-flex align-items-center"
                        data-bs-toggle=""
                        data-i18n="Artikel"
                        id="artikel"
                      >
                        <i data-feather="file-text"></i>
                        <span data-i18n="Artikel">Artikel</span>
                      </a>
                    </li>
                    <li data-menu="" className="">
                      <a
                        className="dropdown-item d-flex align-items-center"
                        data-bs-toggle=""
                        data-i18n="RegulasiASN"
                        id="regulasi"
                      >
                        <i data-feather="file-text"></i>
                        <span data-i18n="RegulasiASN">Regulasi ASN</span>
                      </a>
                    </li>
                    <li data-menu="" className="">
                      <a
                        className="dropdown-item d-flex align-items-center"
                        data-bs-toggle=""
                        data-i18n="Galeri"
                        id="galeri"
                      >
                        <i data-feather="file-text"></i>
                        <span data-i18n="Galeri">Galeri</span>
                      </a>
                    </li>
                    <li data-menu="" className="">
                      <a
                        className="dropdown-item d-flex align-items-center"
                        data-bs-toggle=""
                        data-i18n="StatistikASN"
                        id="statistik"
                      >
                        <i data-feather="file-text"></i>
                        <span data-i18n="StatistikASN">Statistik ASN</span>
                      </a>
                    </li>
                    <li data-menu="" className="">
                      <a
                        className="dropdown-item d-flex align-items-center"
                        data-bs-toggle=""
                        data-i18n="WebSettings"
                        id="settings"
                      >
                        <i data-feather="file-text"></i>
                        <span data-i18n="WebSettings">Web Settings</span>
                      </a>
                    </li>
                  </ul>
                </li>

                {/*<li className="dropdown nav-item" data-menu="dropdown">
                  <a
                    className="dropdown-toggle nav-link d-flex align-items-center"
                    data-bs-toggle="dropdown"
                  >
                    <i data-feather="settings"></i>
                    <span data-i18n="Preferences">Preferences</span>
                  </a>
                  <ul className="dropdown-menu" data-bs-popper="none">
                    <li data-menu="" className="">
                      <a
                        className="dropdown-item d-flex align-items-center"
                        data-bs-toggle=""
                        data-i18n="KategoriKonten"
                        id="kategori"
                      >
                        <i data-feather="file-text"></i>
                        <span data-i18n="KategoriKonten">Kategori Konten</span>
                      </a>
                    </li>
                  </ul>
                </li>*/}

                <li className="dropdown nav-item" data-menu="dropdown">
                  <a
                    className="dropdown-toggle nav-link d-flex align-items-center"
                    data-bs-toggle="dropdown"
                  >
                    <i data-feather="tool"></i>
                    <span data-i18n="settings">Settings</span>
                  </a>
                  <ul className="dropdown-menu" data-bs-popper="none">
                    <li data-menu="" className="">
                      <a
                        className="dropdown-item d-flex align-items-center"
                        data-bs-toggle=""
                        data-i18n="users"
                        id="users"
                      >
                        <i data-feather="user"></i>
                        <span data-i18n="users">Users</span>
                      </a>
                    </li>
                  </ul>
                </li>
                
              </ul>
            </div>
          </div>
        </div>
        {/*<!-- END: Main Menu-->*/}
      </>
    );
  }
}

const mapStateToProps = function (state) {
  return state.configReducers;
};

export default connect(mapStateToProps, {
  setConfig,
  ajaxHandler,
  ajaxViewHandler,
})(withRouter(MenuHorizontal));
