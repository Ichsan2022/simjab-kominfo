import React, { Component }  from 'react';
import Cookies from 'js-cookie';

class Config{
	
	get_site_info(){
		return {
			name: 'CMS Website',
			logo: process.env.PUBLIC_URL + "/kominfo.jpg",
			logo_landscape: process.env.PUBLIC_URL + "/kominfo.jpg",
			description: 'CMS Website'
		}
	}

	get_server_url(){
		const prod = 'https://simasn.kominfo.egov.co.id';
		const dev = 'https://simasn.kominfo.egov.co.id';

		if(Cookies.get("mode") == "prod"){
			return prod;
		}else{
			return dev;
		}
	}
	
	get_base_url(){
		const prod = '#';
		const dev = '#';

		if(Cookies.get("is_prod")){
			return prod;
		}else{
			return dev;
		}
	}

	get_web_socket(){
		return 'wss://';
	}

	get_cookie_name(){
		return 'rscmsjab2_cr4uth';
	}

	get_cookie_time(){
		return 1/24;//1 jam
	}

	stripTags(teks){
		var temp = teks.replace(/&nbsp;/, "");
		return temp.replace(/(<([^>]+)>)/ig, "");//strip tags html
	}

	escape_str(value){
		return(
			<div>
				{value.includes("https://") || value.includes("http://") || value.includes("www.")
					?
						<div>
								<a href={value} target="_blank">{value}</a>
						</div>
					:
						value
				}
			</div>
		);
	}

	cm_to_pixel(cm){
		return cm*37.7952755906;
	}

	getSlug(teks){
		//Peresmian Jalan Tol Serang – Panimbang, Presiden Jokowi: Infrastruktur adalah Fondasi Negara Kita untuk Bersaing Dengan Negara Lain
		var temp = teks.trim();
		temp = temp.replace(/-/g, '');//dash
		temp = temp.replace(/–/g, '');//dash special
		temp = temp.replace(/\s+/g, '-').toLowerCase();//space
		temp = temp.replace(/(<([^>]+)>)/ig, "");//strip tags html
		// temp = temp.replace(/,()/ig, "");//()
		temp = temp.replace(/[,+\/*%@!?&$#^:'"{}()<>:'"]/g, "");
		return encodeURIComponent(temp);
	}
}
export default Config
