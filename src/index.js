import React from 'react';
import ReactDOM from "react-dom/client";
//import './index.css';
import store from './store/store';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';

import { BrowserRouter, Route, Switch, Redirect, Link } from "react-router-dom";
import { Router } from "react-router";

import AuthLayout from "./layouts/AuthLayout.jsx";
import AdminLayout from "./layouts/AdminLayout.jsx";
import {PrivateRoute} from './PrivateRoute';

import { createBrowserHistory } from 'history';


import { 
  Result,
  Button
} from 'antd';

// import { initializeFirebase } from './push-notification';
const history = createBrowserHistory();

const container = document.getElementById('root');
const root = ReactDOM.createRoot(container); // createRoot(container!) if you use TypeScript
root.render(
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route path="/auth" component={AuthLayout} />

        {/*<PrivateRoute path="/admin" component={AdminLayout} />*/}
        <Route path="/admin" component={AdminLayout} />
        
        
        <Route from="/" render={() => <Redirect to="/admin/index"/>}/>{/*live server must off*/}
        {/*<Route path="" render={
            () => 
                <Result
                    status="404"
                    title={
                      <div>
                      404
                      </div>
                    }
                    subTitle={
                      <div>
                        <h1>Maaf, halaman tidak ditemukan</h1><br />
                        <img src={process.env.PUBLIC_URL + "/icon.svg"} height="50" /><br /><br />
                        <Link to="/">
                          <Button type="primary">
                            kembali ke halaman utama
                          </Button>
                        </Link>
                      </div>
                    }
                />
        } />*/}
      </Switch>
    </Router>
  </Provider>
);



// ReactDOM.createRoot(
//   <Provider store={store}>
//   	<BrowserRouter>
//     	<Switch>
//     		<Route path="/auth" component={AuthLayout} />

//         {/*<PrivateRoute path="/admin" component={AdminLayout} />*/}
//         <Route path="/" component={AdminLayout} />
        
        
//     		{/*<Route from="/" render={() => <Redirect to="/admin/index"/>}/>{/*live server must off*/}*/}
//         <Route path="" render={
//             () => 
//                 <Result
//                     status="404"
//                     title={
//                       <div>
//                       404
//                       </div>
//                     }
//                     subTitle={
//                       <div>
//                         <h1>Maaf, halaman tidak ditemukan</h1><br />
//                         <img src={process.env.PUBLIC_URL + "/icon.svg"} height="50" /><br /><br />
//                         <Link to="/">
//                           <Button type="primary">
//                             kembali ke halaman utama
//                           </Button>
//                         </Link>
//                       </div>
//                     }
//                 />
//         } />
//     	</Switch>
//     </BrowserRouter>
//   </Provider>,
//   document.getElementById('root')
// );

// initializeFirebase();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();
