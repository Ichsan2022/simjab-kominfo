import React, { PureComponent } from "react";
import { Redirect, Link } from "react-router-dom";
import { withRouter } from "react-router";
import {
  Button,
  Row,
  Col,
  Input,
  Layout,
  Form,
  Alert,
  Modal,
  message,
  Switch,
  Tabs,
  notification,
  Spin 
} from "antd";
import 'antd/dist/antd.css'; 
// import "@ant-design/compatible/assets/index.css";
import Config from "../Config";
import { Helmet } from "react-helmet";

import { connect } from "react-redux";
import { setConfig, ajaxHandler, ajaxViewHandler } from "../store/actions";

import axios from "axios";
import Cookies from "js-cookie";

const configClass = {
  apiUrl: {
    cek_login: "api/cms_portal/auth/login/",
  },
};

class AuthLayout extends PureComponent {
  config;
  timeoutGetQr;
  timeOutPushQr;

  constructor(props) {
    super(props);
    this.config = new Config();
    this.state = {
      msg: "",
      loading: false,
    };
  }

  componentDidMount() {
    if(document.getElementsByClassName('blank-page').length <= 0){
      document.body.classList.add('blank-page'); 
    }
    // console.log('init', this.props)

  }

  // componentWillReceiveProps(prevProps, nextProps){
  //   console.log("prev", prevProps)
  //   console.log("new", nextProps)
  // }

  doLogin = (values) =>{
    this.postLogin(values)
  };


  async postLogin(datas){
    // console.log('before', this.props)

    this.setState({
        msg: "",
        loading: true
    });

    // this.props.ajaxViewHandler('get', configClass.apiUrl.cek_login, datas)
    this.props.ajaxHandler('post', configClass.apiUrl.cek_login, datas, false, true, "login")
    .then(() =>{
      
      

      setTimeout(() =>{
        if(this.props.isSuccess){
          
            const data = this.props.responseMsg;
            
            Cookies.set(this.config.get_cookie_name()+"_nama", data.data.name != null ? data.data.name : data.data.email, { expires: this.config.get_cookie_time() });
            Cookies.set(this.config.get_cookie_name(), "Bearer "+data.data.access_token, { expires: this.config.get_cookie_time() });
            this.props.setConfig({
              loading: false,
              isLogin: true
            });
          
          // Cookies.set(this.config.get_cookie_name()+"_nama", data.data.name != null ? data.data.name : data.data.email, { expires: this.config.get_cookie_time() });
          // Cookies.set(this.config.get_cookie_name(), "Bearer "+data.data.access_token, { expires: this.config.get_cookie_time() });
          // this.props({
          //   loading: false,
          //   isLogin: true
          // });
        }else{
          
            const data = this.props.responseMsg;
            this.setState({
              msg: data.message != undefined ? (data.data != undefined ? data.data.email[0] : data.message) : JSON.stringify(data),
              loading: false
            });
          
          // console.log('data3', data)
          // this.setState({
          //   msg: data.message != undefined ? data.message : JSON.stringify(data),
          //   loading: false
          // });
        }
      }, 500);
    }).catch((response) => {
      // console.log(response)
      setTimeout(() =>{
        this.setState({
          loading: false,
          msg: JSON.stringify(response)
        });
      }, 500)
    });
  };



  render() {
    if(this.props.isLogin){
      // this.props.history.push("/admin/index")
      // return <Redirect to={"/admin/index"} />
      // this.props.history.push(`/admin/index`, { from: '/auth' } );
      window.location.href = "/admin/index"
    }
    return (
      <>
        {/*<!-- BEGIN: Content-->*/}
        <div className="app-content content ">
          <div className="content-overlay"></div>
          <div className="header-navbar-shadow"></div>
          <div className="content-wrapper">
            <div className="content-header row"></div>
            <div className="content-body">
              <div className="auth-wrapper auth-cover">
                <div className="auth-inner row m-0">
                  {/*<!-- Brand logo-->*/}
                  <a className="brand-logo" href="/">
                    <img 
                      src={process.env.PUBLIC_URL + "/kominfo.jpg"} 
                      height={28}
                    />
                    {/*<svg
                      viewBox="0 0 139 95"
                      version="1.1"
                      xmlns="http://www.w3.org/2000/svg"
                      // xmlns:xlink="http://www.w3.org/1999/xlink"
                      height="28"
                    >
                      <defs>
                        <lineargradient
                          id="linearGradient-1"
                          x1="100%"
                          y1="10.5120544%"
                          x2="50%"
                          y2="89.4879456%"
                        >
                          <stop stopColor="#000000" offset="0%"></stop>
                          <stop stopColor="#FFFFFF" offset="100%"></stop>
                        </lineargradient>
                        <lineargradient
                          id="linearGradient-2"
                          x1="64.0437835%"
                          y1="46.3276743%"
                          x2="37.373316%"
                          y2="100%"
                        >
                          <stop
                            stopColor="#EEEEEE"
                            stopOpacity="0"
                            offset="0%"
                          ></stop>
                          <stop stopColor="#FFFFFF" offset="100%"></stop>
                        </lineargradient>
                      </defs>
                      <g
                        id="Page-1"
                        stroke="none"
                        strokeWidth="1"
                        fill="none"
                        fillRule="evenodd"
                      >
                        <g
                          id="Artboard"
                          transform="translate(-400.000000, -178.000000)"
                        >
                          <g
                            id="Group"
                            transform="translate(400.000000, 178.000000)"
                          >
                            <path
                              className="text-primary"
                              id="Path"
                              d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z"
                              style={{fill: "currentColor"}}
                            ></path>
                            <path
                              id="Path1"
                              d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z"
                              fill="url(#linearGradient-1)"
                              opacity="0.2"
                            ></path>
                            <polygon
                              id="Path-2"
                              fill="#000000"
                              opacity="0.049999997"
                              points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"
                            ></polygon>
                            <polygon
                              id="Path-21"
                              fill="#000000"
                              opacity="0.099999994"
                              points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"
                            ></polygon>
                            <polygon
                              id="Path-3"
                              fill="url(#linearGradient-2)"
                              opacity="0.099999994"
                              points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"
                            ></polygon>
                          </g>
                        </g>
                      </g>
                    </svg>*/}
                    <h2 className="brand-text text-primary ms-1">CMS Website</h2>
                  </a>
                  {/*<!-- /Brand logo-->*/}
                  {/*<!-- Left Text-->*/}
                  <div className="d-none d-lg-flex col-lg-8 align-items-center p-5">
                    <div className="w-100 d-lg-flex align-items-center justify-content-center px-5">
                      <img
                        className="img-fluid"
                        src={process.env.PUBLIC_URL + "/kominfotransparan.png"}
                        alt="Login V2"
                        style={{
                          height: 300
                        }}
                      />
                    </div>
                  </div>
                  {/*<!-- /Left Text-->*/}
                  {/*<!-- Login-->*/}
                  <div className="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5">
                    <div className="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                      <h2 className="card-title fw-bold mb-1">
                        LOGIN CMS WEBSITE
                      </h2>
                      {this.state.msg != "" &&
                        
                          <div className="alert alert-danger alert-dismissible fade show" role="alert">
                            <div className="alert-body d-flex align-items-center">
                              {this.state.msg}
                            </div>
                          </div>
                        
                      }
                      <Spin 
                        tip="Loading..."
                        spinning={this.state.loading}
                      >
                        <Form
                          className="auth-login-form mt-2"
                          onFinish={this.doLogin}
                          // action="index.html"
                          // method="POST"
                        >
                          <div className="mb-1">
                            <label className="form-label" htmlFor="login-email">
                              Email
                            </label>
                            <Form.Item
                              name="email"
                              rules={[{ required: true, message: 'harus diisi!' }, {type: "email", message: 'format email tidak valid'}]}
                            >
                              <Input
                                className="form-control"
                                type="text"
                                placeholder="mail@domain.com"
                                aria-describedby="login-email"
                                autoFocus=""
                                tabIndex="1"
                              />
                            </Form.Item>
                          </div>
                          <div className="mb-1">
                            <label className="form-label" htmlFor="login-email">
                              Password
                            </label>
                            <Form.Item
                              name="password"
                              rules={[{ required: true, message: 'harus diisi!' }]}
                            >
                              <Input.Password
                                className="form-control"
                                type="text"
                                placeholder="******"
                                aria-describedby="login-email"
                                autoFocus=""
                                tabIndex="2"
                              />
                            </Form.Item>
                          </div>
                          {/*<div className="mb-1">
                            <div className="form-check">
                              <input
                                className="form-check-input"
                                id="remember-me"
                                type="checkbox"
                                tabIndex="3"
                              />
                              <label
                                className="form-check-label"
                                htmlFor="remember-me"
                              >
                                {" "}
                                Remember Me
                              </label>
                            </div>
                          </div>*/}
                          <button className="btn btn-primary w-100">
                            Sign in
                          </button>
                        </Form>
                      </Spin>
                      <p className="text-center mt-2">
                        <a href="auth-register-cover.html">
                          <span>&nbsp;Forget Password</span>
                        </a>
                      </p>
                      {/*<div className="divider my-2">
                        <div className="divider-text">or</div>
                      </div>
                      <div className="auth-footer-btn d-flex justify-content-center">
                        <a className="btn btn-facebook" href="#">
                          <i data-feather="facebook"></i>
                        </a>
                        <a className="btn btn-twitter white" href="#">
                          <i data-feather="twitter"></i>
                        </a>
                        <a className="btn btn-google" href="#">
                          <i data-feather="mail"></i>
                        </a>
                        <a className="btn btn-github" href="#">
                          <i data-feather="github"></i>
                        </a>
                      </div>*/}

                    </div>
                  </div>
                  {/*<!-- /Login-->*/}
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*<!-- END: Content-->*/}
      </>
    );
  }
}

const mapStateToProps = function (state) {
  return state.configReducers;
};

export default connect(mapStateToProps, {
  setConfig,
  ajaxHandler,
  ajaxViewHandler,
})(withRouter(AuthLayout));
