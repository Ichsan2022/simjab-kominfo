import React, { PureComponent } from "react";
import { Redirect, Link, Route, Switch } from "react-router-dom";
import { withRouter } from "react-router";
import {
  Button,
  Row,
  Col,
  Input,
  Layout,
  Form,
  Alert,
  Modal,
  message,
  Tabs,
  notification,
} from "antd";
// import "@ant-design/compatible/assets/index.css";
import Config from "../Config";
import { Helmet } from "react-helmet";

import { connect } from "react-redux";
import { setConfig, ajaxHandler, ajaxViewHandler } from "../store/actions";

import axios from "axios";
import Cookies from "js-cookie";

import Header from "../components/HeaderHorizontal.jsx";
import Menu from "../components/MenuHorizontal.jsx";
import Footer from "../components/Footer.jsx";

import NotFound from "../views/NotFound.jsx";
import Home from "../views/Home.jsx";
import DataKategori from "../views/kategori/DataKategori.jsx";
import FormKategori from "../views/kategori/FormKategori.jsx";
import DataPengumuman from "../views/pengumuman/DataPengumuman.jsx";
import FormPengumuman from "../views/pengumuman/FormPengumuman.jsx";
import DataArtikel from "../views/artikel/DataArtikel.jsx";
import FormArtikel from "../views/artikel/FormArtikel.jsx";
import DataRegulasi from "../views/regulasi/DataRegulasi.jsx";
import FormRegulasi from "../views/regulasi/FormRegulasi.jsx";
import DataGaleri from "../views/galeri/DataGaleri.jsx";
import FormGaleri from "../views/galeri/FormGaleri.jsx";
import DataUser from "../views/user/DataUser.jsx";

const configClass = {
  apiUrl: {
    profile: "api/v1.0/user/info/",
  },
};

class AdminLayout extends PureComponent {
  config;
  timeoutGetQr;
  timeOutPushQr;

  constructor(props) {
    super(props);
    this.config = new Config();
    this.state = {
      msg: "",
      loading: false,
    };
  }

  componentDidMount() {
  	// document.body.classList.remove('blank-page'); 
  }

  render() {
    if(this.props.isLogin == false){
      return <Redirect to={"/auth"} />
    }
    return (
      <>
        <Header />

        <Menu />

        <Switch>
        	<Route path="/admin/index" exact component={Home} />
        	<Route path="/admin/preferences/kategori-konten" exact component={DataKategori} />
          <Route path="/admin/preferences/kategori-konten/form/:id?" exact component={FormKategori} />
          <Route path="/admin/cms/pengumuman" exact component={DataPengumuman} />
          <Route path="/admin/cms/pengumuman/form/:id?" exact component={FormPengumuman} />
          <Route path="/admin/cms/artikel" exact component={DataArtikel} />
          <Route path="/admin/cms/artikel/form/:id?" exact component={FormArtikel} />
          <Route path="/admin/cms/regulasi-asn" exact component={DataRegulasi} />
          <Route path="/admin/cms/regulasi-asn/form/:id?" exact component={FormRegulasi} />
          <Route path="/admin/cms/galeri" exact component={DataGaleri} />
          <Route path="/admin/cms/galeri/form/:id?" exact component={FormGaleri} />
          <Route path="/admin/settings/user" exact component={DataUser} />

        	<Route path="" components={NotFound} />
        </Switch>

        <div className="sidenav-overlay"></div>
        <div className="drag-target"></div>

        <Footer />
      </>
    );
  }
}

const mapStateToProps = function (state) {
  return state.configReducers;
};

export default connect(mapStateToProps, {
  setConfig,
  ajaxHandler,
  ajaxViewHandler,
})(withRouter(AdminLayout));
