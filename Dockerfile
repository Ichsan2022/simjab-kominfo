FROM node:alpine AS deps
RUN apk add --update bash && rm -rf /var/cache/apk/*
WORKDIR /app
COPY package*.json ./

# Stage 2: build
FROM node:alpine AS builder
WORKDIR /app
COPY public ./public
COPY package*.json firebase.json .firebaserc ./
COPY ./ ./


# Stage 3: run
FROM node:alpine
WORKDIR /app
COPY --from=builder /app/public ./public
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./
CMD ["npm" , "start"]
